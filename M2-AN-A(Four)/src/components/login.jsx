import React, { Component } from "react";
import httpServices from "./httpServices";
class Login extends Component {
  state = {
    loginDetail: { email: "", password: "" },
    user: {},
    details: false,
    errors: {},
  };
  componentDidMount() {
    let user = localStorage.getItem("user");
    if (user) {
      localStorage.clear();
      window.location = "/login";
    }
  }
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.loginDetail[input.name] = input.value;
    console.log(s1.loginDetail);
    this.handleValidate(e);
    this.setState(s1);
  };
  async fetchData() {
    const user = localStorage.getItem("user");
    console.log(user);
    if (user) {
      let { data: response } = await httpServices.get("/user", {
        headers: { Authorization: user },
      });
      localStorage.setItem("currentUser", JSON.stringify(response));
      window.location =
        response.role === "Admin"
          ? "/adminPage"
          : response.role === "Student"
          ? "/studentPage"
          : response.role === "Faculty"
          ? "/facultyPage"
          : "";
    }
  }
  async postData(url, obj) {
    console.log(url, obj);
    try {
      let { data: response, headers } = await httpServices.post(url, obj);
      console.log(response);
      let token = headers["x-auth-token"];
      console.log(`Token :: ${token}`);
      localStorage.setItem("user", `bearer ${token}`);
      this.fetchData();
    } catch (err) {
      alert("Invalid User Details or network error !!");
    }
  }
  handleSubmit = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    let errors = this.validateAll();
    if (this.isValid(errors)) {
      this.postData("/user", s1.loginDetail);
    } else {
      s1.errors = errors;
      this.setState(s1);
    }
  };
  isFormValid = () => {
    let errors = this.validateAll();
    return this.isValid(errors);
  };
  isValid = (errors) => {
    let keys = Object.keys(errors);
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
    return count === 0;
  };
  validateAll = () => {
    let { email, password } = this.state.loginDetail;
    let errors = {};
    errors.email = this.validateEmail(email);
    errors.password = this.validatePassword(password);
    return errors;
  };
  handleValidate = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    switch (input.name) {
      case "email":
        s1.errors.email = this.validateEmail(input.value);
        break;
      case "password":
        s1.errors.password = this.validatePassword(input.value);
        break;
      default:
        break;
    }
  };
  validateEmail = (email) => (email ? "" : "Email is required");
  validatePassword = (password) =>
    password && password.length > 6
      ? ""
      : "Password must be of atleast 7 characters";
  render() {
    const { errors, loginDetail } = this.state;
    let { email, password } = loginDetail;
    return (
      <div className="container text-center">
        <h2>Login</h2>
        <div className="row">
          <div className="col-1"></div>
          <div className="col-2">
            Email<b className="text-danger">*</b>
          </div>
          <div className="form-group col-6">
            <input
              className="form-control"
              type="text"
              name="email"
              value={email}
              placeholder="Enter your email"
              onChange={this.handleChange}
            />
            <br />
            {errors.email ? (
              <div
                className="text-danger col-12"
                style={{ backgroundColor: "pink" }}
              >
                {errors.email}
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="col-3"></div>
          <div className="col-1"></div>
          <div className="col-2">
            Password<b className="text-danger">*</b>
          </div>
          <div className="col-6 form-group">
            <input
              className="form-control"
              type="password"
              name="password"
              value={password}
              placeholder="Enter your Password"
              onChange={this.handleChange}
            />
            {errors.password ? (
              <div
                className="text-danger col-12"
                style={{ backgroundColor: "pink" }}
              >
                {errors.password}
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="col-3"></div>
        </div>
        <button
          className="btn btn-primary mt-1 btn-sm"
          disabled={!this.isFormValid()}
          onClick={this.handleSubmit}
        >
          Login
        </button>
      </div>
    );
  }
}
export default Login;
