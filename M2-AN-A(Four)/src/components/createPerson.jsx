import React, { Component } from "react";
import httpServices from "./httpServices";
class CreatePerson extends Component {
  state = {
    user: { name: "", password: "", cpassword: "", email: "", role: "" },
    errors: {},
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.user[input.name] = input.value;
    this.handleValidate(e);
    this.setState(s1);
  };
  async postData(url, obj) {
    try {
      let response = await httpServices.post(url, obj);
      let { status } = response;
      console.log(response);
      if (status === 200) {
        alert("User created successfully !!");
        this.props.history.push('/adminPage');
        // window.location = "/admin";
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 500) {
        alert("Add a valid user details !!");
      }
    }
  }
  handleCreate = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    let errors = this.validateAll();
    if (this.isValid(errors)) {
      let data = {};
      data.name = s1.user.name;
      data.email = s1.user.email;
      data.password = s1.user.password;
      data.role = s1.user.role;
      this.postData("/createUser", data);
    } else {
      s1.errors = errors;
      this.setState(s1);
    }
  };
  isFormValid = () => {
    let errors = this.validateAll();
    return this.isValid(errors);
  };
  isValid = (errors) => {
    let keys = Object.keys(errors);
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
    return count === 0;
  };
  validateAll = () => {
    let { name, password, cpassword, email, role } = this.state.user;
    let errors = {};
    errors.name = this.validateName(name);
    errors.password = this.validatePassword(password);
    errors.cpassword = this.validateCPassword(cpassword, password);
    errors.email = this.validateEmail(email);
    errors.role = this.validateRole(role);
    return errors;
  };
  handleValidate = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    switch (input.name) {
      case "name":
        s1.errors.name = this.validateName(input.value);
        break;
      case "password":
        s1.errors.password = this.validatePassword(input.value);
        break;
      case "cpassword":
        s1.errors.cpassword = this.validateCPassword(
          input.value,
          s1.user.password
        );
        break;
      case "email":
        s1.errors.email = this.validateEmail(input.value);
        break;
      case "role":
        s1.errors.role = this.validateRole(input.value);
        break;
      default:
        break;
    }
  };
  validateName = (name) => (name ? "" : "Name is required");
  validatePassword = (password) =>
    password && password.length > 6
      ? ""
      : "Password is required. Length sholud be atleast 7 chars";
  validateCPassword = (password1, password) =>
    password1
      ? password1 === password
        ? ""
        : "Passwords does not match"
      : "Password is required";
  validateEmail = (email) => (email ? "" : "Email is required");
  validateRole = (role) => (role ? "" : "Role is required");
  render() {
    const { name, password, cpassword, email, role } = this.state.user;
    const { errors } = this.state;
    return (
      <div className="container">
        <br/><h3>Create a New User</h3>
        <div className="form-group">
          <label>
            Name<b className="text-danger">*</b>
          </label>
          <input
            className="form-control"
            type="text"
            name="name"
            value={name}
            placeholder="Enter Customer Name"
            onChange={this.handleChange}
          />
          {errors.name ? (
            <div className="text-danger col-12" style={{backgroundColor: "pink"}}>{errors.name}</div>
          ) : (
            ""
          )}
        </div>
        <div className="form-group">
          <label>
            Password<b className="text-danger">*</b>
          </label>
          <input
            className="form-control"
            type="password"
            name="password"
            value={password}
            placeholder="Enter password"
            onChange={this.handleChange}
          />
          {errors.password ? (
            <div className="text-danger col-12" style={{backgroundColor: "pink"}}>{errors.password}</div>
          ) : (
            ""
          )}
        </div>
        <div className="form-group">
          <label>
            Confirm Password<b className="text-danger">*</b>
          </label>
          <input
            className="form-control"
            type="password"
            name="cpassword"
            value={cpassword}
            placeholder="Re-Enter Password"
            onChange={this.handleChange}
          />
          {errors.cpassword ? (
            <div className="text-danger col-12" style={{backgroundColor: "pink"}}>{errors.cpassword}</div>
          ) : (
            ""
          )}
        </div>
        <div className="form-group">
          <label>
            Email<b className="text-danger">*</b>
          </label>
          <input
            className="form-control"
            type="text"
            name="email"
            value={email}
            placeholder="Enter your email"
            onChange={this.handleChange}
          />
          {errors.email ? (
            <div className="text-danger col-12" style={{backgroundColor: "pink"}}>{errors.email}</div>
          ) : (
            ""
          )}
        </div>
        <label>
          Role<b className="text-danger">*</b>
        </label>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="role"
              value="student"
              checked={role === "student"}
              onChange={this.handleChange}
            />
            <label className="form-check-label">Student</label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="role"
              value="faculty"
              checked={role === "faculty"}
              onChange={this.handleChange}
            />
            <label className="form-check-label">Faculty</label>
          </div>
          {errors.role ? (
            <div className="text-danger col-12" style={{backgroundColor: "pink"}}>{errors.role}</div>
          ) : (
            ""
          )}
        <button
          className="btn btn-primary btn-sm"
          disabled={!this.isFormValid()}
          onClick={this.handleCreate}
        >
          Register
        </button>
      </div>
    );
  }
}
export default CreatePerson;
