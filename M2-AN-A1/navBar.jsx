import React, { Component } from "react";
import { Link } from "react-router-dom";
class NavBar extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
        <Link className="navbar-brand" to="/">
          MyCompany
        </Link>
        <div className="" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link className="nav-link" to="/emps">
                All
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={`/emps/${"New Delhi"}`}>
                New Delhi
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={`/emps/${"Noida"}`}>
                Noida
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
export default NavBar;
