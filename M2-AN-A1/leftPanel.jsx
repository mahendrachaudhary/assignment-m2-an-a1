import React, { Component } from "react";
class LeftPanel extends Component {
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let options = { ...this.props.options };
    console.log(input.type);
    if (input.type === "checkbox") {
      options[input.name] = this.updateCBs(
        options[input.name],
        input.checked,
        input.value
      );
    } else {
      options[input.name] = input.value;
    }
    this.props.onOptionChange(options);
  };
  updateCBs = (inpValue, checked, value) => {
    console.log(value);
    let inpArr = inpValue ? inpValue.split(",") : [];
    if (checked) inpArr.push(value);
    else {
      let index = inpArr.findIndex((ele) => ele === value);
      if (index >= 0) inpArr.splice(index, 1);
    }
    return inpArr.join(",");
  };
  makeCheckboxes = (arr, values, name, label) => (
    <React.Fragment>
      <label className="form-check-label font-weight-bold">{label}</label>
      {arr.map((opt) => (
        <div className="form-check" key={opt}>
          <input
            className="form-check-input"
            value={opt}
            type="checkbox"
            name={name}
            checked={values.find((val) => val === opt) || false}
            onChange={this.handleChange}
          />
          <label className="form-check-label">{opt}</label>
        </div>
      ))}
    </React.Fragment>
  );
  makeRadios = (arr, values, name, label) => (
    <React.Fragment>
      <label className="form-check-label font-weight-bold">{label}</label>
      {arr.map((opt) => (
        <div className="form-check" key={opt}>
          <input
            className="form-check-input"
            value={opt}
            type="radio"
            name={name}
            checked={values === opt}
            onChange={this.handleChange}
          />
          <label className="form-check-label">{opt}</label>
        </div>
      ))}
    </React.Fragment>
  );
  render() {
    const { designations, departments } = this.props;
    const { designation = "", department = "" } = this.props.options;
    return (
      <React.Fragment>
        {this.makeRadios(
          designations,
          designation,
          "designation",
          "Designation"
        )}
        {this.makeCheckboxes(
          departments,
          department.split(","),
          "department",
          "Department"
        )}
      </React.Fragment>
    );
  }
}
export default LeftPanel;
