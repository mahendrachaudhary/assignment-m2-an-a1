import React, { Component } from "react";
import LeftPanel from "./leftPanel";
import queryString from "query-string";
class AllEmps extends Component {
  filterParams = (arr, queryParams) => {
    let { designation, department } = queryParams;
    arr = this.filterParam(arr, "department", department);
    arr = designation ? arr.filter((e) => e.designation === designation) : arr;
    return arr;
  };
  filterParam = (arr, name, values) => {
    if (!values) return arr;
    let valuesArr = values.split(",");
    let arr1 = arr.filter((a1) => valuesArr.find((val) => val === a1[name]));
    return arr1;
  };
  addToQueryString = (str, paramName, paramValue) =>
    paramValue
      ? str
        ? `${str}&${paramName}=${paramValue}`
        : `${paramName}=${paramValue}`
      : str;

  makeSearchString = (options) => {
    let { page, designation, department } = options;
    let searchStr = "";
    searchStr = this.addToQueryString(searchStr, "designation", designation);
    searchStr = this.addToQueryString(searchStr, "department", department);
    searchStr = this.addToQueryString(searchStr, "page", page);
    return searchStr;
  };
  handlePage = (incr) => {
    let queryParams = queryString.parse(this.props.location.search);
    let { city } = this.props.match.params;
    let { page = "1" } = queryParams;
    let newPage = +page + incr;
    queryParams.page = newPage;
    let url = city ? `/emps/${city}` : "/emps";
    this.callURL(url, queryParams);
  };
  handleOptionChange = (options) => {
    let { city } = this.props.match.params;
    let url = city ? `/emps/${city}` : "/emps";
    options.page = "1";
    this.callURL(url, options);
  };
  callURL = (url, options) => {
    let searchString = this.makeSearchString(options);
    this.props.history.push({
      pathname: url,
      search: searchString,
    });
  };
  render() {
    const { city } = this.props.match.params;
    let { emps } = this.props;
    let emps1 = city ? emps.filter((e) => e.location === city) : emps;
    let queryParams = queryString.parse(this.props.location.search);
    let { page = "1" } = queryParams;
    let pageNum = +page;
    let emps2 = this.filterParams(emps1, queryParams);
    let startIndex = pageNum - 1;
    let endIndex = pageNum;
    let designations = emps.reduce(
      (acc, curr) =>
        acc.find((d) => d === curr.designation)
          ? acc
          : [...acc, curr.designation],
      []
    );
    let departments = emps.reduce(
      (acc, curr) =>
        acc.find((d) => d === curr.department)
          ? acc
          : [...acc, curr.department],
      []
    );
    let emps3 =
      emps2.length > 2
        ? emps2.filter((l, index) => index >= startIndex && index <= endIndex)
        : emps2;

    return (
      <div className="container-fluid row">
        <div className="col-3">
          <LeftPanel
            designations={designations}
            departments={departments}
            options={queryParams}
            onOptionChange={this.handleOptionChange}
          />
        </div>
        <div className="col-9 row">
          <div className="col-12">
            <h4 className="text-center">Welcome to the Employee Portal</h4>
            <div className="col-12">
              <h6>You have chosen</h6>
              Location : {city ? city : "All"}
              <br />
              Department :{" "}
              {queryParams.department ? queryParams.department : "All"}
              <br />
              Designation :{" "}
              {queryParams.designation ? queryParams.designation : "All"}
              <br />
            </div>
            <br />
            <div className="col-12">
              <div className="col-12">
                The number of employees matching the options :{emps2.length}
              </div>
              <div className="col-12 row">
                {emps3.map((e, index) => (
                  <div className="col-6 border" key={index}>
                    <b>{e.name}</b>
                    <br />
                    {e.email}
                    <br />
                    {e.mobile}
                    <br />
                    {e.location}
                    <br />
                    {e.department}
                    <br />
                    {e.designation}
                    <br />
                    {e.salary}
                    <br />
                  </div>
                ))}
              </div>
              <div className="row">
                <div className="col-2">
                  {startIndex > 0 ? (
                    <button
                      className="btn btn-primary btn-sm"
                      onClick={() => this.handlePage(-1)}
                    >
                      Prev
                    </button>
                  ) : (
                    ""
                  )}
                </div>
                <div className="col-8"></div>
                <div className="col-2">
                  {endIndex < emps2.length - 1 ? (
                    <button
                      className="btn btn-primary btn-sm"
                      onClick={() => this.handlePage(1)}
                    >
                      Next
                    </button>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default AllEmps;
