import React, { Component } from "react";
import httpServices from "./httpServices";
class Logout extends Component {
  async componentDidMount() {
    const user = localStorage.getItem("user");
    
    if (user && localStorage.removeItem("currentUser")) {
      let { data: response } = await httpServices.deleteReq(
        "/logout",
        { headers: { Authorization: user } },
        "/"
      );
    }
  }
  render() {
    return "";
  }
}
export default Logout;
