import React, { Component } from "react";
import { Link } from "react-router-dom";
class NavBar extends Component {
  render() {
    const currentUser = localStorage.getItem("currentUser");
    const user = JSON.parse(currentUser);
    console.log(user);
    return (
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
        <Link className="navbar-brand text-light" to="/">
          <b>
            {user && user.role === "student"
              ? "Student Home"
              : user && user.role === "Faculty"
              ? "Faculty Home"
              : user && user.role === "Admin"
              ? "Admin Home"
              : "Training Institute"}
          </b>
        </Link>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            {user && user.role === "Admin" && (
              <li className="nav-item">
                <Link className="nav-link text-light" to="/createPerson">
                  Create Person
                </Link>
              </li>
            )}
            {user && user.role === "Admin" && (
              <li className="nav-item">
                <Link className="nav-link text-light" to="/allPerson">
                  View All Person
                </Link>
              </li>
            )}
            {user && user.role === "Admin" && (
              <li className="nav-item">
                <Link className="nav-link text-light" to="/register">
                  Create Course
                </Link>
              </li>
            )}
            {user && user.role === "Admin" && (
              <li className="nav-item">
                <Link className="nav-link text-light" to="/register">
                  View All Course
                </Link>
              </li>
            )}
            {user && user.role === "Student" && (
              <li className="nav-item">
                <Link className="nav-link text-light" to="/studentDetails">
                  View All Courses
                </Link>
              </li>
            )}
            {user && user.role === "Student" && (
              <li className="nav-item">
                <Link className="nav-link text-light" to="/allClasses">
                  View All Lectures
                </Link>
              </li>
            )}
            {user && user.role === "Faculty" && (
              <li className="nav-item">
                <Link className="nav-link text-light" to="/allCourses">
                  View Courses
                </Link>
              </li>
            )}
            {user && user.role === "Faculty" && (
              <li className="nav-item">
                <Link className="nav-link text-light" to="/allLectures">
                  View Lectures
                </Link>
              </li>
            )}
            {user && user.role === "Faculty" && (
              <li className="nav-item">
                <Link className="nav-link text-light" to="/createLecture">
                  Create Lecture
                </Link>
              </li>
            )}
            {user && user.role === "Faculty" && (
              <li className="nav-item">
                <Link className="nav-link text-light" to="/coursesAssigned">
                  Mark Attendance
                </Link>
              </li>
            )}
          </ul>
          <ul className="navbar-nav ml-auto">
            {user && (
              <li className="nav-item">
                <Link className="nav-link">
                  Welcome {user.role === "admin" ? "Admin" : user.name}
                </Link>
              </li>
            )}
            {user && (
              <li className="nav-item">
                <Link className="nav-link text-light" to="/logout">
                  Logout
                </Link>
              </li>
            )}
            {!user && (
              <li className="nav-item">
                <Link className="nav-link" to="login">
                  Login
                </Link>
              </li>
            )}
          </ul>
        </div>
      </nav>
    );
  }
}
export default NavBar;
