import axios from "axios";

const baseURL = "http://localhost:2410";

function get(url, headers) {
    return axios.get(baseURL + url, headers);
}

function post(url, obj) {
    return axios.post(baseURL + url, obj);
}
function put(url, obj) {
    return axios.put(baseURL + url, obj);
}
function deleteReq(url, headers, path) {
    return axios.delete(baseURL + url, headers, path);
}

export default {
    get,
    post,
    put,
    deleteReq,
};