import React, {Component} from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import AdminPage from './adminPage';
import CreatePerson from './createPerson';
import Login from './login';
import NavBar from './navBar';
import FacultyPage from './facultyPage';
import StudentPage from './studentPage';
class MainComp extends Component {
    render() {
        return <React.Fragment>
            <NavBar />
            <Switch>
                <Route path="/createPerson" component={CreatePerson} />
                <Route path="/facultyPage" component={FacultyPage} />
                <Route path="/studentPage" component={StudentPage} />
                <Route path="/adminPage" component={AdminPage} />
                <Route path="/login" component={Login} />
                <Redirect to='/login' from='/' />
            </Switch>
        </React.Fragment>
    }
}
export default MainComp;