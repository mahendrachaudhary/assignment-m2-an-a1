import React, { Component } from "react";
import {Link} from "react-router-dom";
class NavBar extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-sm navbar-light bg-light">
        <Link className="navbar-brand" to="/">
        <i class="fas fa-book-open"></i>
        </Link>
        <div className="" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link className="nav-link" to={`/books?searchText=${"Harry Potter"}`}>
                Harry Potter
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={`/books?searchText=${"Agatha Christie"}`}>
                Agatha Christie
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={`/books?searchText=${"Premchand"}`}>
                Premchand
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={`/books?searchText=${"Jane Austen"}`}>
                Jane Austen
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={`/books/${"management"}`}>
                My Books
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/books">
                Settings
              </Link>
            </li>            
          </ul>
         </div>
      </nav>
    );
  }
}
export default NavBar;
