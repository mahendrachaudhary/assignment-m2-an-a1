import React, {Component} from "react";
import { Redirect, Switch, Route } from "react-router-dom";
import Books from "./books";
import Home from "./home";
import NavBar from "./navBar";
class MainComp extends Component {
    render() {
        return <React.Fragment>
            <NavBar />
            <Switch>
                <Route path="/books" component={Books} />
                <Route path="/home" component={Home} />
                <Redirect from="/" to="/home" />
            </Switch>
        </React.Fragment>
    }
}
export default MainComp;