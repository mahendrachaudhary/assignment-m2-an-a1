import React, { Component } from "react";
import Pic from "./img1.jpg";
class Home extends Component {
    state = {
        search: "",
    }
  render() {
      const {search} = this.state;
    return (
      <div className="text-center">
        <img
          src={Pic}
          style={{
            borderRadius: "50%",
            height: "280px",
            marginTop: "20px",
          }}
        />
        <div className="row mt-1 col-12">
          <div className="col-2"></div>
          <div className="col-7">
            <div className="form group">
              <input
                className="form-control"
                type="text"
                name="search"
                value={search}
                placeholder="Search"
                onChange={this.hanldeChange}
              />
            </div>
          </div>
          <div className="col-2">
              <button className="btn btn-primary btn-sm">Search</button>
          </div>
          <div className="col-3"></div>
        </div>
      </div>
    );
  }
}
export default Home;
