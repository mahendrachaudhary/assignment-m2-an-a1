import React, {Component} from "react";
import queryString from "query-string";
import httpService from "./httpService";
class Books extends Component {
    state={
        data: {},
    }
    async componentDidMount() {
        let queryParams = queryString.parse(this.props.location.search);
        queryParams.startIndex = "0";
        queryParams.maxResults = "8";
        console.log(queryParams);
        let searchStr = this.makeSearchString(queryParams);
        console.log(searchStr);
        let response = await httpService.get(`${searchStr}`);
        console.log(response);
    }
    callURL = (url, options) => {
        let searchString = this.makeSearchString(options);
        this.props.history.push({
          pathname: url,
          search: searchString,
        });
      };
      makeSearchString = (options) => {
        let { searchText, startIndex, maxResults, minAge } = options;
        console.log(options);
        let searchStr = "";
        searchStr = this.addToQueryString(searchStr, "searchText", searchText);
        searchStr = this.addToQueryString(searchStr, "startIndex", startIndex);
        searchStr = this.addToQueryString(searchStr, "maxResults", maxResults);
        searchStr = this.addToQueryString(searchStr, "minAge", minAge);
        return searchStr;
      };
      addToQueryString = (str, paramName, paramValue) =>
        paramValue
          ? str
            ? `${str}&${paramName}=${paramValue}`
            : `${paramName}=${paramValue}`
          : str;
    
    render() {
        return "";
    }
}
export default Books;