import React, { Component } from "react";
import { Link } from "react-router-dom";
import httpService from "./httpService";
import queryString from "query-string";
class Category extends Component {
  state = {
    data: {},
    orderByOpt: ["None", "newest", "oldest", "relevance"],
    sections: ["Business", "Technology", "Politics", "LifeStyle"],
    section: "",
    orderBy: "",
    search: "",
    prevSearch: "",
  };
  async fetchData() {
    let queryParams = queryString.parse(this.props.location.search);
    let s1 = {...this.state};
    let searchStr = this.makeSearchString(queryParams);
    let response =
      queryParams.q
        ? await httpService.get(`?q=${searchStr}&${"api-key"}=${"test"}`)
        : "";
    const { data } = response;
    s1.data = data;
    if(s1.search !== queryParams.q){
      s1.section = "";
      s1.orderBy = "None";
    }
    s1.search = queryParams.q;
    
    this.setState(s1);
  }
  componentDidMount() {
    this.fetchData();
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) this.fetchData();
  }
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1[input.name] = input.value;

    if(input.name === "orderBy" || input.name === "section") {
      let queryParams = queryString.parse(this.props.location.search);
      if(input.name=== "orderBy") {
        queryParams.orderBy = input.value; 
        queryParams.page = 1;
      }
      if(input.name=== "section") {
        queryParams.section = input.value.toLowerCase();
        queryParams.page = 1;
      }
      this.callURL("/home", queryParams);
    }
    this.setState(s1);
  };
  handleSubmit = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    let queryParams = queryString.parse(this.props.location.search);
    queryParams.q = s1.search;
    queryParams.orderBy = "None";
    this.callURL("/home", queryParams);
  };
  handlePage = (incr) => {
    let queryParams = queryString.parse(this.props.location.search);
    console.log(queryParams);
    let { page = "1" } = queryParams;
    let newPage = +page + incr;
    queryParams.page = newPage;
    this.callURL("/home", queryParams);
  };
  callURL = (url, options) => {
    let searchString = this.makeSearchString(options);
    this.props.history.push({
      pathname: url,
      search: searchString,
    });
  };
  makeSearchString = (options) => {
    let { q, page, orderBy, section } = options;
    let searchStr = "";
    searchStr = this.addToQueryString(searchStr, "q", q);
    searchStr = this.addToQueryString(searchStr, "page", page);
    searchStr = this.addToQueryString(searchStr, "orderBy", orderBy);
    searchStr = this.addToQueryString(searchStr, "section", section);
    return searchStr;
  };
  addToQueryString = (str, paramName, paramValue) =>
    paramValue
      ? str
        ? `${str}&${paramName}=${paramValue}`
        : `${paramName}=${paramValue}`
      : str;
  render() {
    const { data = {} } = this.state;
    const { response = {} } = data;
    const { results = [] } = response;
    let { orderBy, section, sections, orderByOpt, search } = this.state;
    return (
      <div>
        <br />
        <br />
        <div className="row container">
          <div className="col-3">
            <div className="col-12 border bg-light pt-2 pb-2">
              <b>Order By</b>
            </div>
            <div className="col-12 form-group p-0">
              <select
                className="form-control"
                name="orderBy"
                value={orderBy}
                onChange={this.handleChange}
              >
                <option disabled value="">
                  Order By
                </option>
                {orderByOpt.map((opt) => (
                  <option>{opt}</option>
                ))}
              </select>
            </div>
            <br />
            <hr />
            <div className="col-12 p-0">
              <label className="col-12 border bg-light font-weight-bold pt-2 pb-2 m-0">
                Sections
              </label>
              {sections.map((opt) => (
                <div className="form-check border pt-2 pb-2">
                  <input
                    className="form-check-input ml-0"
                    name="section"
                    value={opt}
                    type="radio"
                    checked={opt === section}
                    onChange={this.handleChange}
                  />
                  <label
                    className="form-check-label"
                    style={{ marginLeft: "20px" }}
                  >
                    {opt}
                  </label>
                </div>
              ))}
            </div>
          </div>
          <div className="col-9">
            <div className="row">
              <div className="col-10">
                <input
                  className="form-control"
                  type="text"
                  name="search"
                  value={search}
                  placeholder="Enter Search Text"
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-2">
                <button className="btn btn-light" onClick={this.handleSubmit}>
                  Submit
                </button>
              </div>
            </div>
            {data.response
              ? response.startIndex +
                "-" +
                (response.startIndex === 0 ? 0 : response.startIndex + 9) +
                " of " +
                response.total
              : ""}
            <div className="row col-12">
              {data.response
                ? results.map((d) => (
                    <div
                      className="col-4 text-center text-dark"
                      style={{
                        backgroundColor: "#03a9f436",
                        border: "5px solid white",
                      }}
                    >
                      {d.webTitle}
                      <br />
                      <b>{d.sectionName}</b>
                      <br />
                      <b>{d.webPublicationDate}</b>
                      <br />
                      <Link to={d.webUrl}>Preview</Link>
                    </div>
                  ))
                : ""}
            </div>
            {data.response ? (
              <div className="row col-12">
                <div className="col-2 text-center">
                  {response.currentPage > 1 ? (
                    <button
                      className="btn btn-danger btn-sm"
                      onClick={() => this.handlePage(-1)}
                    >
                      Prev
                    </button>
                  ) : (
                    ""
                  )}
                </div>
                <div className="col-8"></div>
                <div className="col-2">
                  {response.currentPage < response.pages ? (
                    <button
                      className="btn btn-danger btn-sm"
                      onClick={() => this.handlePage(1)}
                    >
                      Next
                    </button>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
      </div>
    );
  }
}
export default Category;
