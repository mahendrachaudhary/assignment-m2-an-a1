import React, {Component} from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Category from "./category";
import NavBar from "./navBar";
class Home extends Component {

    render() {
        return <React.Fragment>
            <NavBar />
            <Switch>
                {/* <Route path="/home/:category" component={Category} /> */}
                <Route path="/home" component={Category} />
                <Redirect from="/" to="/home" />
            </Switch>
        </React.Fragment>
    }
}
export default Home;