import React, { Component } from "react";
import {Link} from "react-router-dom";
class NavBar extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-sm navbar-dark bg-danger">
        <Link className="navbar-brand text-dark" to="/">
          NewsSite
        </Link>
        <div className="" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link className="nav-link" to={`/home?q=${"sports"}&page=${1}`}>
                Sports
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={`/home?q=${"cricket"}&page=${1}`}>
                Cricket
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={`/home?q=${"movies"}&page=${1}`}>
                Movies
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={`/home?q=${"education"}&page=${1}`}>
                Education
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
export default NavBar;
