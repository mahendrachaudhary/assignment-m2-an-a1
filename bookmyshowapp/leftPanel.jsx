import React, { Component } from "react";
class LeftPanel extends Component {
  state = {
    languages: ["Hindi", "English", "Punjabi", "Tamil"],
    formats: ["2D", "3D", "4DX"],
    genres: ["Action", "Adventure", "Biography", "Comedy"],
    langStatus: true,
    formatStatus: false,
    genreStatus: false,
  };
  handleLang = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    s1.langStatus = !s1.langStatus;
    this.setState(s1);
  };
  handleFormat = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    s1.formatStatus = !s1.formatStatus;
    this.setState(s1);
  };
  handleGenre = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    s1.genreStatus = !s1.genreStatus;
    this.setState(s1);
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let options = { ...this.props.options };
    if (input.type === "checkbox")
      options[input.name] = this.updateCBs(
        options[input.name],
        input.checked,
        input.value
      );
    else options[input.name] = input.value;
    this.props.onOptionChange(options);
  };
  updateCBs = (inpValues, checked, value) => {
    let inpArr = inpValues ? inpValues.split(",") : [];
    if (checked) inpArr.push(value);
    else {
      let index = inpArr.findIndex((ele) => ele === value);
      if (index >= 0) inpArr.splice(index, 1);
    }
    return inpArr.join(",");
  };

  render() {
    const {
      languages,
      formats,
      genres,
      langStatus,
      formatStatus,
      genreStatus,
    } = this.state;
    let { lang = "", format = "", genre = "" } = this.props.options;
    return (
      <div className="container">
        <div className="mb-4 bg-white p-2" style={{ borderRadius: "3%" }}>
          <label
            className={`form-check-label ${
              langStatus ? "text-primary" : "text-dark"
            }`}
            style={{ width: "100%" }}
          >
            {langStatus ? (
              <span>
                <i className="fas fa-chevron-up" onClick={this.handleLang}></i>
              </span>
            ) : (
              <span>
                <i className="fas fa-chevron-down" onClick={this.handleLang}></i>
              </span>
            )}
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select Language
          </label>
          {langStatus
            ? languages.map((opt) => (
                <div className="form-check" key={opt}>
                  <input
                    className="form-check-input"
                    value={opt}
                    type="checkbox"
                    name="lang"
                    checked={lang.split(",").find((val) => val === opt)}
                    onChange={this.handleChange}
                  />
                  <label className="form-check-label">{opt}</label>
                </div>
              ))
            : ""}
        </div>
        <div className="mb-4 bg-white p-2" style={{ borderRadius: "3%" }}>
          <label
            className={`form-check-label ${
              formatStatus ? "text-primary" : "text-dark"
            }`}
            style={{ width: "100%" }}
          >
            {formatStatus ? (
              <span>
                <i className="fas fa-chevron-up" onClick={this.handleFormat}></i>
              </span>
            ) : (
              <span>
                <i className="fas fa-chevron-down" onClick={this.handleFormat}></i>
              </span>
            )}
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Format
          </label>
          {formatStatus
            ? formats.map((opt) => (
                <div className="form-check" key={opt}>
                  <input
                    className="form-check-input"
                    value={opt}
                    type="checkbox"
                    name="format"
                    checked={format.split(",").find((val) => val === opt)}
                    onChange={this.handleChange}
                  />
                  <label className="form-check-label">{opt}</label>
                </div>
              ))
            : ""}
        </div>
        <div className="mb-4 bg-white p-2" style={{ borderRadius: "3%" }}>
          <label
            className={`form-check-label ${
              genreStatus ? "text-primary" : "text-dark"
            }`}
            style={{ width: "100%" }}
          >
            {genreStatus ? (
              <span>
                <i className="fas fa-chevron-up" onClick={this.handleGenre}></i>
              </span>
            ) : (
              <span>
                <i className="fas fa-chevron-down" onClick={this.handleGenre}></i>
              </span>
            )}
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Genre
          </label>
          {genreStatus
            ? genres.map((opt) => (
                <div className="form-check" key={opt}>
                  <input
                    className="form-check-input"
                    value={opt}
                    type="checkbox"
                    name="genre"
                    checked={genre.split(",").find((val) => val === opt)}
                    onChange={this.handleChange}
                  />
                  <label className="form-check-label">{opt}</label>
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }
}
export default LeftPanel;
