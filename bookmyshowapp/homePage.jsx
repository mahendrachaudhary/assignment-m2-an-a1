import React, { Component } from "react";
import Carousel from "react-bootstrap/Carousel";
import httpServices from "./httpServices";
import { Link } from "react-router-dom";
import queryString from 'query-string';
import LeftPanel from './leftPanel';
class HomePage extends Component {
  state = {
    moviesList: [],
  };
  async fetchData() {
    let { city } = this.props;
    let queryParams = queryString.parse(this.props.location.search);
    let searchStr = this.makeSearchString(queryParams);
    console.log(queryParams);
    let response = await httpServices.get(`/movies/${city}?${searchStr}`);
    const { data } = response;
    this.setState({ moviesList: data });
  }
  componentDidMount() {
    this.fetchData();
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) this.fetchData();
  }
  makeSearchString = (options) => {
    let { lang, format, genre } = options;
    let searchStr = "";
    searchStr = this.addToQueryString(searchStr, "lang", lang);
    searchStr = this.addToQueryString(searchStr, "format", format);
    searchStr = this.addToQueryString(searchStr, "genre", genre);
    console.log(searchStr);
    return searchStr;
  };
  addToQueryString = (str, paramName, paramValue) =>
    paramValue
      ? str
        ? `${str}&${paramName}=${paramValue}`
        : `${paramName}=${paramValue}`
      : str;
  handleOptionChange = (options) => {
    let { city } = this.props;
    console.log(options, city);
    this.callURL(`/home/${city}`, options);
  };
  callURL = (url, options) => {
    let searchString = this.makeSearchString(options);
    console.log(url, searchString);
    this.props.history.push({
      pathname: url,
      search: searchString,
    });
  };
  handleMovieSlots = (index) => {
    let { city } = this.props;
    this.props.history.push(`/home/${city}/${index}`)
  }
  render() {
    const { moviesList = [] } = this.state;
    let queryParams = queryString.parse(this.props.location.search);
    console.log(moviesList);
    return (
      <>
        <div className="row"><br/>
          <div className="col-2"></div>
          <div className="col-8">
            <Carousel>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src="https://i.ibb.co/ZGsJ3dh/jio-mami-21st-mumbai-film-festival-with-star-2019-02-09-2019-10-58-45-992.png"
                  alt="First slide"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src="https://i.ibb.co/wRr7W1P/hustlers-01-10-2019-05-09-55-486.png"
                  alt="Second slide"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src="https://i.ibb.co/qFWPRpF/laal-kaptaan-16-10-2019-12-48-06-721.jpg"
                  alt="Third slide"
                />
              </Carousel.Item>
            </Carousel>
          </div>
          <div className="col-2"></div>
        </div>
        <nav className="container navbar navbar-expand-sm navbar-dark" style={{background: "#ebecec"}}>
          <Link className="navbar-brand text-dark" to="#" style={{ fontSize: "28px" }}>
            Movies
          </Link>
          <div className="navbar" id="navbarSupportedContent">
            <ul className="navbar-nav">
              <li className="nav-item">
                <Link className="nav-link text-dark" to="#">Now Showing</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link text-dark" to="#">Coming Soon</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link text-dark" to="#">Exclusive</Link>
              </li>
            </ul>
          </div>
        </nav>
        <div className="row" style={{background: "#ebecec"}}>
          <div className="col-3">
            <div className="col-10">
              <img
                className="ml-2 mt-1 p-2 bg-white"
                src="https://i.ibb.co/Hry1kDH/17443322900502723126.jpg"
              />
            </div><br/><br/>
            <LeftPanel options={queryParams} onOptionChange={this.handleOptionChange} />
          </div>
          <div className="col-9">
            <div className="row ml-3 mt-1">
              {moviesList.map((m, index) => (
                <div className="col-3 ml-1 mr-1 bg-white" onClick={()=>this.handleMovieSlots(index)}>
                  <div className="row">
                    <div className="col-12 bg-white">
                      <img
                        className="img-fluid"
                        style={{
                          borderTopLeftRadius: "5%",
                          borderTopRightRadius: "5%",
                        }}
                        src={m.img}
                      />
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="row">
                      <div className="col-4">
                        <div className="row" style={{ fontSize: "14px" }}>
                          <span style={{ color: "red" }}>
                            <i className="fas fa-heart"></i>
                          </span>
                          {m.rating}
                        </div>
                        <div
                          className="row text-muted"
                          style={{ fontSize: "12px" }}
                        >
                          {m.votes} votes
                        </div>
                      </div>
                      <div className="col-8">
                        <div className="row" style={{ fontSize: "14px" }}>
                          {m.title}
                        </div>
                        <div
                          className="row text-muted"
                          style={{ fontSize: "13px" }}
                        >
                          {m.desc}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default HomePage;
