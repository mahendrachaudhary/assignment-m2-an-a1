import React, { Component } from "react";
import "./login.css";
import Desclaimer from "./desclaimer";

class Smile extends Component {
  render() {
    return (
      <div
        style={{
          background:
            "url( //in.bmscdn.com/webin/static/book-a-smile/showcase/banner11.jpg)",
          paddingBottom: "50px",
        }}
      >
        <div
          className="row"
          style={{
            color: "#fff",
            fontSize: "40px",
            paddingLeft: "180px",
            paddingTop: "150px",
            paddingBottom: "150px",
            fontWeight: 100,
          }}
        >
          <div className="col">
            <h3>#BookMyShowCares.</h3>
          </div>
        </div>
        <br />
        <div
          className="row"
          style={{
            backgroundColor: "#f2f2f2",
            fontFamily: "#f2f2f2",
            padding: "50px",
          }}
        >
          <div className="col">
            <div
              className="row"
              style={{ backgroundColor: "#a8121e", padding: "50px 0" }}
            >
              <div className="col">
                <h3
                  style={{
                    float: "left",
                    width: "100%",
                    textAlign: "center",
                    fontSize: "42px",
                    color: "#fff",
                    fontFamily: "Jenna Sue, cursive",
                    marginBottom: "30px",
                  }}
                >
                  Where is it going
                </h3>
                <div className="row">
                  <div className="col">
                    <img
                      src=" //in.bmscdn.com/webin/static/book-a-smile/afail.jpg"
                      style={{
                        opacity: 1,
                        width: "177px",
                        marginRight: "20px",
                        height: "75px",
                      }}
                    />
                    <img
                      src=" //in.bmscdn.com/webin/static/book-a-smile/artreach.jpg"
                      style={{
                        opacity: 1,
                        width: "177px",
                        marginRight: "20px",
                        height: "75px",
                      }}
                    ></img>
                    <img
                      src=" //in.bmscdn.com/webin/static/book-a-smile/antarang.jpg"
                      style={{
                        opacity: 1,
                        width: "177px",
                        marginRight: "20px",
                        height: "75px",
                      }}
                    ></img>
                    <img
                      src=" //in.bmscdn.com/webin/static/book-a-smile/astha.jpg"
                      style={{
                        opacity: 1,
                        width: "177px",
                        marginRight: "20px",
                        height: "75px",
                      }}
                    ></img>
                    <img
                      src=" //in.bmscdn.com/webin/static/book-a-smile/advitya.png"
                      style={{
                        opacity: 1,
                        width: "177px",
                        marginRight: "20px",
                        height: "75px",
                      }}
                    ></img>
                    <img
                      src="//in.bmscdn.com/webin/static/book-a-smile/akansha.png"
                      style={{
                        opacity: 1,
                        width: "177px",
                        marginRight: "20px",
                        height: "75px",
                      }}
                    ></img>
                  </div>
                </div>
              </div>
            </div>
            <div className="row" style={{ paddingBottom: "35px" }}>
              <div className="col text-center">
                <h2
                  style={{
                    color: "#2c3946",
                    paddingTop: "20px",
                    marginTop: "40px",
                    fontSize: "18px",
                  }}
                >
                  IN THE NEWS
                </h2>
                <div
                  style={{
                    width: "30px",
                    height: "2px",
                    background: "#c80910",
                    margin: "20px auto 0",
                  }}
                ></div>
                <div className="row">
                  <div className="col">
                    <img
                      class=""
                      src="//in.bmscdn.com/webin/static/bms-news/adgully.png"
                      style={{
                        opacity: 1,
                        width: "177px",
                        marginRight: "20px",
                        height: "75px",
                      }}
                    ></img>
                    <img
                      class=""
                      src="//in.bmscdn.com/webin/static/bms-news/afternoon.png"
                      style={{
                        opacity: 1,
                        width: "177px",
                        marginRight: "20px",
                        height: "75px",
                      }}
                    ></img>
                    <img
                      class=""
                      src="//in.bmscdn.com/webin/static/bms-news/business-standard.png"
                      style={{
                        opacity: 1,
                        width: "177px",
                        marginRight: "20px",
                        height: "75px",
                      }}
                    ></img>
                    <img
                      class=""
                      src="//in.bmscdn.com/webin/static/bms-news/business-line.png"
                      style={{
                        opacity: 1,
                        width: "177px",
                        marginRight: "20px",
                        height: "75px",
                      }}
                    ></img>
                    <img
                      class=""
                      src="//in.bmscdn.com/webin/static/bms-news/bollyspice.png"
                      style={{
                        opacity: 1,
                        width: "177px",
                        marginRight: "20px",
                        height: "75px",
                      }}
                    ></img>
                    <img
                      class=""
                      src="//in.bmscdn.com/webin/static/bms-news/deccan-herald.png"
                      style={{
                        opacity: 1,
                        width: "177px",
                        marginRight: "20px",
                        height: "75px",
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col">
                <h3
                  style={{
                    fontSize: "18px",
                    color: "#666",
                    marginBottom: "20px",
                    paddingTop: "35px",
                  }}
                >
                  Disclaimer
                </h3>
                <ul>
                  <li
                    style={{
                      marginRight: "10px",
                      fontSize: "12px",
                      color: "#666",
                      lineHeight: "24px",
                      listStyleType: "decimal",
                    }}
                  >
                    Contributions once made cannot be refunded or cancelled.
                  </li>
                  <li
                    style={{
                      marginRight: "10px",
                      fontSize: "12px",
                      color: "#666",
                      lineHeight: "24px",
                      listStyleType: "decimal",
                      wordWrap: "normal",
                    }}
                  >
                    Big Tree Entertainment Pvt Ltd (BEPL) is facilitating the
                    transactions on the platform
                    https://in.bookmyshow.com/donation. The proceeds of the same
                    will be used for social initiatives for the underprivileged
                    sections of society.
                  </li>
                  <li
                    style={{
                      marginRight: "10px",
                      fontSize: "12px",
                      color: "#666",
                      lineHeight: "24px",
                      listStyleType: "decimal",
                    }}
                  >
                    Apart from this, BEPL is not engaged in any partnership,
                    association or tie-up with the NGOs.
                  </li>
                  <li
                    style={{
                      marginRight: "10px",
                      fontSize: "12px",
                      color: "#666",
                      lineHeight: "24px",
                      listStyleType: "decimal",
                    }}
                  >
                    BEPL will not be responsible for End use of the funds
                    donated.
                  </li>
                  <li
                    style={{
                      marginRight: "10px",
                      fontSize: "12px",
                      color: "#666",
                      lineHeight: "24px",
                      listStyleType: "decimal",
                    }}
                  >
                    BEPL expressly disclaims any and all liability and assumes
                    no responsibility whatsoever for consequences resulting from
                    any actions or inactions of the NGO.
                  </li>
                  <li
                    style={{
                      marginRight: "10px",
                      fontSize: "12px",
                      color: "#666",
                      lineHeight: "24px",
                      listStyleType: "decimal",
                    }}
                  >
                    By proceeding to donate the money, you do so at your own
                    risk and expressly waive any and all claims, rights of
                    action and/or remedies (under law or otherwise) that you may
                    have against BEPL arising out of or in connection with the
                    aforesaid transaction.
                  </li>
                  <li
                    style={{
                      marginRight: "10px",
                      fontSize: "12px",
                      color: "#666",
                      lineHeight: "24px",
                      listStyleType: "decimal",
                      width: "92%",
                    }}
                  >
                    BEPL will not be held responsible for the issuance of 80G
                    certificate.
                  </li>
                  <li
                    style={{
                      marginRight: "10px",
                      fontSize: "12px",
                      color: "#666",
                      lineHeight: "24px",
                      listStyleType: "decimal",
                    }}
                  >
                    For any queries, kindly{" "}
                    <a
                      target="_blank"
                      href="https://support.bookmyshow.com/support/tickets/new"
                    >
                      email us
                    </a>
                    .
                  </li>
                </ul>
              </div>
            </div>
            <br />
            <br />
            <Desclaimer />
          </div>
        </div>
      </div>
    );
  }
}

export default Smile;
