import React, { Component } from "react";
import axios from "axios";
import queryString from "query-string";
// import { Link } from "react-router-dom";

class SeatArrangement extends Component {
  state = {
    amount: 0,
    seatNo: "",
    allData: {},
    totalTicket: 0
  };

  async componentWillMount() {
    const id = this.props.match.params.id;
    const city = this.props.match.params.city;
    const cIndex = this.props.match.params.cIndex;
    const timeIndex = this.props.match.params.timeIndex;
    const date = this.props.match.params.date;
    let { time } = queryString.parse(this.props.location.search);

    let apiEndPint =
      "https://us-central1-bkyow-22da6.cloudfunctions.net/app/movies/";
    apiEndPint = apiEndPint + city + "/" + id;

    let apiEndPintForSeat =
      "https://us-central1-bkyow-22da6.cloudfunctions.net/app/seats";

    const { data: moviesData } = await axios.get(apiEndPint);
    const { data: seatData } = await axios.get(apiEndPintForSeat);
    this.setState({ seatData: seatData, moviesData: moviesData });
  }

  async componentDidUpdate(prevProps, prevState, snapshot) {
    let currProps = this.props;
    const id = this.props.match.params.id;
    const city = this.props.match.params.city;
    const cIndex = this.props.match.params.cIndex;
    const timeIndex = this.props.match.params.timeIndex;
    const date = this.props.match.params.date;
    let { time } = queryString.parse(this.props.location.search);

    let apiEndPint =
      "https://us-central1-bkyow-22da6.cloudfunctions.net/app/movies/";
    apiEndPint = apiEndPint + city + "/" + id;

    let apiEndPintForSeat =
      "https://us-central1-bkyow-22da6.cloudfunctions.net/app/seats";

    if (currProps !== prevProps) {
      console.log("check prevProps and currProps", currProps === prevProps);
      const { data: moviesData } = await axios.get(apiEndPint);
      const { data: seatData } = await axios.get(apiEndPintForSeat);
      this.setState({ seatData: seatData, moviesData: moviesData });
    }
  }

  bookSeat = (mainindex, rowName, seatIndex, seatNo, price, add) => {
    const timeIndex = this.props.match.params.timeIndex;
    let ind = timeIndex % 3;
    let seatData = [...this.state.seatData];
    let seats = seatData[ind].seats;
    if (add === 1) {
      seats[mainindex].seatList[seatIndex].booked = !seats[mainindex].seatList[
        seatIndex
      ].booked;

      seatData[ind].seats = seats;
      this.state.amount += price;
      let seatBook = this.state.seatNo;
      this.state.totalTicket += +1;
      seatBook += rowName + seatNo + " ";
      this.props.sendData(this.state.allData);
      this.setState({ seatData: seatData, seatNo: seatBook });
    }
    if (add === -1) {
      seats[mainindex].seatList[seatIndex].booked = !seats[mainindex].seatList[
        seatIndex
      ].booked;

      seatData[ind].seats = seats;
      this.state.amount -= price;
      let seatBook = this.state.seatNo;

      this.state.totalTicket += -1;
      let seatCancel = rowName + seatNo;
      let newSeat = seatBook.split(" ");
      let i = newSeat.indexOf(seatCancel);
      newSeat.splice(i, 1);
      seatBook = newSeat.join(" ");
      this.props.sendData(this.state.allData);
      this.setState({ seatData: seatData, seatNo: seatBook });
    }
  };

  handleSave = () => {
    const id = this.props.match.params.id;
    const city = this.props.match.params.city;
    this.props.history.push(`/home/${city}/${id}`);
  };

  handleChangeTime = index => {
    const id = this.props.match.params.id;
    const city = this.props.match.params.city;
    const cIndex = this.props.match.params.cIndex;
    const date = this.props.match.params.date;
    let { time } = queryString.parse(this.props.location.search);

    let path = "/bookMovie/";
    path += city + "/" + id + "/buyTicket/" + cIndex + "/" + index + "/" + date;
    let params = "?time=" + time;
    this.state.totalTicket = 0;
    this.state.amount = 0;
    this.props.history.push({ pathname: path, search: params });
  };
  handleClick = async () => {
    let token = localStorage.getItem("user");
    console.log(token);
    let userData;
    let apiEndPoint = "https://us-central1-myhub-17a13.cloudfunctions.net/app/user";
    try {
       userData  = await axios.get(apiEndPoint, {
        headers: { Authorization: token },
      });
      console.log(userData)
    } catch (e) {
      //alert(e);
      alert("Login to Proceed!!!");
      this.props.history.push("/");
    }
    if(userData)
    this.props.history.push('/payment')

  }

  render() {
    const id = this.props.match.params.id;
    const city = this.props.match.params.city;
    const cIndex = this.props.match.params.cIndex;
    const timeIndex = this.props.match.params.timeIndex;
    const date = this.props.match.params.date;
    let { time } = queryString.parse(this.props.location.search);
    let seats = [];
    let name = "";
    let movieTime = "";
    let ind = timeIndex % 3;
    if (this.state.seatData) {
      seats = this.state.seatData[ind].seats;
      name = this.state.moviesData.showTiming[date][cIndex].name;
      movieTime = this.state.moviesData.showTiming[date][cIndex].timings[
        timeIndex
      ].name;
      this.state.allData.city = city;
      this.state.allData.movieId = id;
      this.state.allData.dateId = date;
      this.state.allData.cId = cIndex;
      this.state.allData.timeId = timeIndex;
      this.state.allData.time = time;
      this.state.allData.movie = this.state.moviesData.title;
      this.state.allData.hall = name;
      this.state.allData.total = this.state.totalTicket;
      this.state.allData.tickets = this.state.seatNo;
      this.state.allData.date = time;
      this.state.allData.time = movieTime;
      this.state.allData.amount = this.state.amount;

      this.state.allData.votes = this.state.moviesData.votes;
    }
    return (
      <div>
        {this.state.seatData && (
          <div>
            <div className="bg-dark ">
              <div className="row text-white">
                <div className=" d-xs-block col-12 col-lg-6  mt-1">
                  <span
                    className="ml-3"
                    onClick={() => this.handleSave()}
                    style={{ cursor: "pointer", fontSize: "14px" }}
                  >
                    <i className="fa fa-chevron-left" aria-hidden="true" />
                  </span>
                  <span className="ml-4" style={{ fontSize: "25px" }}>
                    {this.state.moviesData.title}
                  </span>
                  <br />
                  <span
                    className="ml-5"
                    style={{ fontSize: "13px", margin: "10px" }}
                  >
                    {name}
                  </span>
                </div>
                <div className="d-none d-lg-block col-6  mt-4 text-right ">
                  <span className="mr-2">{this.state.totalTicket} Tickets</span>
                  <span
                    className="mr-3"
                    onClick={() => this.handleSave()}
                    style={{ cursor: "pointer" }}
                  >
                    <i className="fa fa-times" aria-hidden="true" />
                  </span>
                </div>
              </div>
              <br />
            </div>
            <div className="bg-light">
              <div className="ml-4 p-2">
                <span
                  className="d-xs-block text-md-left text-center text-lg-teft"
                  style={{ fontSize: "14px" }}
                >
                  {time}, {movieTime}
                </span>
                <br />
                <div className="row">
                  {this.state.moviesData.showTiming[date][cIndex].timings.map(
                    (m, index) => (
                      <div key={index}>
                        {m.name === movieTime ? (
                          <button
                            type="button"
                            className="btn btn-success btn-sm m-1"
                            onClick={() => this.handleChangeTime(index)}
                          >
                            {m.name}
                          </button>
                        ) : (
                          <button
                            type="button"
                            className="btn btn-outline-success btn-sm m-1"
                            onClick={() => this.handleChangeTime(index)}
                          >
                            {m.name}
                          </button>
                        )}
                      </div>
                    )
                  )}
                </div>
              </div>
            </div>
            <div className="container">
              Recliner -Rs 420.00
              <hr className="myhr-4"></hr>
              {seats.map((s, index) => (
                <div className=" ml-4 mb-2" key={index}>
                  {s.price == 420 && (
                    <div className="row">
                      <span className=" mr-2">{s.rowName}</span>
                      {s.seatList.map((s1, ind) => (
                        <div
                          className=""
                          key={s1.seatNo}
                          style={{ fontSize: "2px" }}
                        >
                          {s1.available === true ? (
                            <span>
                              {s1.booked === false ? (
                                <button
                                  className="btn btn-sm border ml-1 btn text-center"
                                  style={{
                                    width: "10px",
                                    fontSize: "10px",
                                    textAlign: "center",
                                    width: '21px',
                                    height: '21px',
                                    borderRadius: '5px',
                                    marginTop: '4px',
                                    marginRight: '2px',
                                    marginBottom: '4px',
                                    marginLeft: '2px',
                                   
                                  }}
                                  onClick={() =>
                                    this.bookSeat(
                                      index,
                                      s.rowName,
                                      ind,
                                      s1.seatNo,
                                      s.price,
                                      +1
                                    )
                                  }
                                >
                                  <span style={{ paddingRight: '3px'}}>{s1.seatNo}</span>
                                </button>
                              ) : (
                                <button
                                  className="btn btn-sm border ml-1 btn text-center"
                                  style={{
                                    width: "10px",
                                    fontSize: "10px",
                                    textAlign: "center",
                                    backgroundColor: "#2dc492",
                                    width: '21px',
                                    height: '21px',
                                    borderRadius: '5px',
                                    marginTop: '4px',
                                    marginRight: '2px',
                                    marginBottom: '4px',
                                    marginLeft: '2px',
                                  }}
                                  onClick={() =>
                                    this.bookSeat(
                                      index,
                                      s.rowName,
                                      ind,
                                      s1.seatNo,
                                      s.price,
                                      -1
                                    )
                                  }
                                >
                                  {s1.seatNo}
                                </button>
                              )}
                            </span>
                          ) : (
                            <button
                              className="btn btn-sm border ml-1 btn btn-light text-center"
                              style={{
                                width: "10px",
                                fontSize: "10px",
                                textAlign: "center",
                                width: '21px',
                                height: '21px',
                                borderRadius: '5px',
                                marginTop: '4px',
                                marginRight: '2px',
                                marginBottom: '4px',
                                marginLeft: '2px',
                              }}
                              disabled
                            >
                              {s1.seatNo}
                            </button>
                          )}
                        </div>
                      ))}
                    </div>
                  )}
                </div>
              ))}
              <br />
              Gold -Rs 250.00
              <hr className="myhr-4"></hr>
              <div className="row">
                <div className="col-3">
                  {seats.map((s, index) => (
                    <div className="ml-4 mb-2" key={index}>
                      {s.price == 250 && (
                        <div className="row">
                          <span className=" mr-2">{s.rowName}</span>
                          {s.seatList.map((s1, ind) => (
                            <div className="row" key={s1.seatNo}>
                              {s1.seatNo < 8 && (
                                <div
                                  className="col-3"
                                  style={{ fontSize: "2px" }}
                                >
                                  {s1.available === true ? (
                                    <span>
                                      {s1.booked === false ? (
                                        <button
                                          className="btn btn-sm border ml-1 btn text-center"
                                          style={{
                                            width: "7px",
                                            fontSize: "10px",
                                            textAlign: "center",
                                            width: '21px',
                                            height: '21px',
                                            borderRadius: '5px',
                                            marginTop: '4px',
                                            marginRight: '2px',
                                            marginBottom: '4px',
                                            marginLeft: '2px',
                                          }}
                                          onClick={() =>
                                            this.bookSeat(
                                              index,
                                              s.rowName,
                                              ind,
                                              s1.seatNo,
                                              s.price,
                                              +1
                                            )
                                          }
                                        >
                                          {s1.seatNo}
                                        </button>
                                      ) : (
                                        <button
                                          className="btn btn-sm border ml-1 btn text-center"
                                          style={{
                                            width: "10px",
                                            fontSize: "10px",
                                            textAlign: "center",
                                            backgroundColor: "#2dc492",
                                            width: '21px',
                                            height: '21px',
                                            borderRadius: '5px',
                                            marginTop: '4px',
                                            marginRight: '2px',
                                            marginBottom: '4px',
                                            marginLeft: '2px',
                                          }}
                                          onClick={() =>
                                            this.bookSeat(
                                              index,
                                              s.rowName,
                                              ind,
                                              s1.seatNo,
                                              s.price,
                                              -1
                                            )
                                          }
                                        >
                                          {s1.seatNo}
                                        </button>
                                      )}
                                    </span>
                                  ) : (
                                    <button
                                      className="btn btn-sm border ml-1 btn btn-light text-center"
                                      style={{
                                        width: "10px",
                                        fontSize: "10px",
                                        textAlign: "center",
                                        width: '21px',
                                        height: '21px',
                                        borderRadius: '5px',
                                        marginTop: '4px',
                                        marginRight: '2px',
                                        marginBottom: '4px',
                                        marginLeft: '2px',
                                      }}
                                      disabled
                                    >
                                      {s1.seatNo}
                                    </button>
                                  )}
                                </div>
                              )}
                            </div>
                          ))}
                        </div>
                      )}
                    </div>
                  ))}
                </div>
                <div className="col-1"></div>
                <div className="col-4">
                  {seats.map((s, index) => (
                    <div className="ml-4 mb-2" key={index}>
                      {s.price == 250 && (
                        <div className="row">
                          {s.seatList.map((s1, ind) => (
                            <div className="" key={s1.seatNo}>
                              {s1.seatNo < 19 && s1.seatNo > 7 && (
                                <div
                                  className="col-0"
                                  style={{ fontSize: "2px" }}
                                >
                                  {s1.available === true ? (
                                    <span>
                                      {s1.booked === false ? (
                                        <button
                                          className="btn btn-sm border ml-1 btn text-center"
                                          style={{
                                            width: "7px",
                                            fontSize: "10px",
                                            textAlign: "center",
                                            width: '21px',
                                            height: '21px',
                                            borderRadius: '5px',
                                            marginTop: '4px',
                                            marginRight: '2px',
                                            marginBottom: '4px',
                                            marginLeft: '2px',
                                          }}
                                          onClick={() =>
                                            this.bookSeat(
                                              index,
                                              s.rowName,
                                              ind,
                                              s1.seatNo,
                                              s.price,
                                              +1
                                            )
                                          }
                                        >
                                          {s1.seatNo}
                                        </button>
                                      ) : (
                                        <button
                                          className="btn btn-sm border ml-1 btn text-center"
                                          style={{
                                            width: "10px",
                                            fontSize: "10px",
                                            textAlign: "center",
                                            backgroundColor: "#2dc492",
                                            width: '21px',
                                            height: '21px',
                                            borderRadius: '5px',
                                            marginTop: '4px',
                                            marginRight: '2px',
                                            marginBottom: '4px',
                                            marginLeft: '2px',
                                          }}
                                          onClick={() =>
                                            this.bookSeat(
                                              index,
                                              s.rowName,
                                              ind,
                                              s1.seatNo,
                                              s.price,
                                              -1
                                            )
                                          }
                                        >
                                          {s1.seatNo}
                                        </button>
                                      )}
                                    </span>
                                  ) : (
                                    <button
                                      className="btn btn-sm border ml-1 btn btn-light text-center"
                                      style={{
                                        width: "10px",
                                        fontSize: "10px",
                                        textAlign: "center",
                                        width: '21px',
                                        height: '21px',
                                        borderRadius: '5px',
                                        marginTop: '4px',
                                        marginRight: '2px',
                                        marginBottom: '4px',
                                        marginLeft: '2px',
                                      }}
                                      disabled
                                    >
                                      {s1.seatNo}
                                    </button>
                                  )}
                                </div>
                              )}
                            </div>
                          ))}
                        </div>
                      )}
                    </div>
                  ))}
                </div>
                <div className="col-1"></div>
                <div className="col-3 ">
                  {seats.map((s, index) => (
                    <div className="ml-4 mb-2" key={index}>
                      {s.price == 250 && (
                        <div className="row">
                          {s.seatList.map((s1, ind) => (
                            <div className="" key={s1.seatNo}>
                              {s1.seatNo > 18 && (
                                <div
                                  className="col-0"
                                  style={{ fontSize: "2px" }}
                                >
                                  {s1.available === true ? (
                                    <span>
                                      {s1.booked === false ? (
                                        <button
                                          className="btn btn-sm border ml-1 btn text-center"
                                          style={{
                                            width: "7px",
                                            fontSize: "10px",
                                            textAlign: "center",
                                            width: '21px',
                                            height: '21px',
                                            borderRadius: '5px',
                                            marginTop: '4px',
                                            marginRight: '2px',
                                            marginBottom: '4px',
                                            marginLeft: '2px',
                                          }}
                                          onClick={() =>
                                            this.bookSeat(
                                              index,
                                              s.rowName,
                                              ind,
                                              s1.seatNo,
                                              s.price,
                                              +1
                                            )
                                          }
                                        >
                                          {s1.seatNo}
                                        </button>
                                      ) : (
                                        <button
                                          className="btn btn-sm border ml-1 btn text-center"
                                          style={{
                                            width: "10px",
                                            fontSize: "10px",
                                            textAlign: "center",
                                            backgroundColor: "#2dc492",
                                            width: '21px',
                                            height: '21px',
                                            borderRadius: '5px',
                                            marginTop: '4px',
                                            marginRight: '2px',
                                            marginBottom: '4px',
                                            marginLeft: '2px',
                                          }}
                                          onClick={() =>
                                            this.bookSeat(
                                              index,
                                              s.rowName,
                                              ind,
                                              s1.seatNo,
                                              s.price,
                                              -1
                                            )
                                          }
                                        >
                                          {s1.seatNo}
                                        </button>
                                      )}
                                    </span>
                                  ) : (
                                    <button
                                      className="btn btn-sm border ml-1 btn btn-light text-center"
                                      style={{
                                        width: "10px",
                                        fontSize: "10px",
                                        textAlign: "center",
                                        width: '21px',
                                        height: '21px',
                                        borderRadius: '5px',
                                        marginTop: '4px',
                                        marginRight: '2px',
                                        marginBottom: '4px',
                                        marginLeft: '2px',
                                      }}
                                      disabled
                                    >
                                      {s1.seatNo}
                                    </button>
                                  )}
                                </div>
                              )}
                            </div>
                          ))}
                        </div>
                      )}
                    </div>
                  ))}
                </div>
              </div>
            </div>
            <br />
            <div className="row">
              <div className="col-12 text-center">
                <svg
                  xml={"preserve"}
                  enableBackground="new 0 0 100 100"
                  viewBox="0 0 260 20"
                  y="0px"
                  x="0px"
                  width="260px"
                  height="20px"
                  xmlns={"http://www.w3.org/1999/xlink"}
                  xmlns="http://www.w3.org/2000/svg"
                  version="1.1"
                  style={{ fill: "rgba(0, 0, 0, 0.6)" }}
                >
                  <g fill="none" fillRule="evenodd" opacity=".3">
                    <g fill="#E1E8F1">
                      <path id="da" d="M27.1 0h205.8L260 14.02H0z"></path>
                    </g>
                    <path
                      stroke="#4F91FF"
                      strokeWidth=".65"
                      d="M27.19.33L1.34 13.7h257.32L232.81.32H27.2z"
                    ></path>
                    <path
                      fill="#8FB9FF"
                      d="M28.16 2.97h203.86l17.95 9.14H10.35z"
                    ></path>
                    <g fill="#E3ECFA">
                      <path id="db" d="M0 13.88h260l-3.44 6.06H3.44z"></path>
                    </g>
                    <path
                      stroke="#4F91FF"
                      strokeWidth=".65"
                      d="M.56 14.2l3.07 5.41h252.74l3.07-5.4H.56z"
                    ></path>
                  </g>
                </svg>
                <br />
                <span>All Eyes this way please</span>
                <br />
              </div>
            </div>
            <div className="row fixed-bottom">
              {" "}
              <div className="col-4"></div>
              <div className="col-4">
                {this.state.amount !== 0 && (
                  
                    <div className="">
                      <button className="btn btn-block btn-primary " onClick= {()=>this.handleClick()}>
                        Pay Rs.{this.state.amount}
                      </button>
                    </div>
                 
                )}
              </div>
              <div className="col-4"></div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default SeatArrangement;
