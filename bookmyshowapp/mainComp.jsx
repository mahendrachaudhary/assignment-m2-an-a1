import React, { Component } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import HomePage from "./homePage";
import MovieSlots from "./movieSlots";
import NavBar from "./navBar";
import Payment from "./payment";
import SeatArrangement from "./seatArrangement";
class MainComp extends Component {
  state = {
    city: "NCR",
    allData: {
      city: "",
      movieId: "",
      cId: "",
      timeId: "",
      dateId: "",
      time: "",
      movie: "",
      hall: "",
      total: "",
      tickets: "",
      date: "",
      time: "",
      amount: "",
      votes: "",
    },
  };
  getCity = (str) => {
    this.setState({ city: str });
  };
  handleData = (data) => {
    console.log("main", data);
    this.state.allData = data;
  };
  render() {
    let { city } = this.state;
    return (
      <React.Fragment>
        {/* <NavBar handleCity={this.getCity} city={city} /> */}
        <Switch>
          <Route
            path="/bookMovie/:city/:id/buyTicket/:cIndex/:timeIndex/:date"
            render={(props) => (
              <SeatArrangement sendData={this.handleData} {...props} />
            )}
          />
          <Route
            path="/payment"
            render={(props) => (
              <Payment allData={this.state.allData} {...props} />
            )}
          />
          <Route
            path="/home/:city/:index"
            render={(props) => (
              <React.Fragment>
                <NavBar handleCity={this.getCity} city={city} />
                <MovieSlots {...props} />
              </React.Fragment>
            )}
          />
          <Route
            path="/home/:city"
            render={(props) => (
              <React.Fragment>
                <NavBar handleCity={this.getCity} city={city} />
                <HomePage {...props} city={city} />
              </React.Fragment>
            )}
          />
          <Redirect to="/home/NCR" from="/" />
        </Switch>
      </React.Fragment>
    );
  }
}
export default MainComp;
