import React, { Component } from "react";
import httpServices from "./httpServices";
import "./dd.css";
import { Link } from "react-router-dom";
class MovieSlots extends Component {
  state = {
    movie: {},
    date: new Date(),
    prices: ["0-100", "101-200", "201-300", "More than 300"],
    timings: ["Morning", "Afternoon", "Evening", "Night"],
    price: [],
    time: [],
    btn: 0,
  };
  async componentDidMount() {
    let { index } = this.props.match.params;
    let response = await httpServices.get(`/movies/NCR/${index}`);
    const { data } = response;
    this.setState({ movie: data });
  }
  handleDay = (num) => {
    this.setState({ btn: num });
  };
  render() {
    let {
      movie = {},
      date,
      prices,
      timings,
      price = [],
      time = [],
      btn,
    } = this.state;
    const id = this.props.match.params.index;
    const city = this.props.match.params.city;
    let day = btn;
    const monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];
    console.log(movie);
    return (
      <div>
        <div className="bg-secondary">
          <div className="text-white pt-5">
            <div className="pl-3">
              <h3>{movie.title}</h3>
            </div>
          </div>
          <div className="text-white pl-3">
            <i className="fas fa-heart" style={{ color: "#d6181f" }}></i>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <span style={{ fontSize: "20px" }}>
              <strong>{movie.rating}</strong>
            </span>
            &nbsp;&nbsp;&nbsp;&nbsp;
            {movie.genre
              ? movie.genre.split(",").map((g) => (
                  <>
                    <span className="border rounded-pill p-1">
                      <small>{g.toUpperCase()}</small>
                    </span>
                    &nbsp;&nbsp;
                  </>
                ))
              : ""}
          </div>
          <div className="text-white pl-3">
            <small>{movie.votes} votes</small>
          </div>
        </div>
        <div className="row bg-light pt-1 pb-1">
          <div className=" col-5 pl-3">
            <button
              type="button"
              className={`btn btn-sm m-3 ${
                btn === 0 ? "btn-success" : "btn-light"
              }`}
              onClick={() => this.handleDay(0)}
            >
              {date.getDate()} TODAY
            </button>
            <button
              type="button"
              className={`btn btn-sm m-3 ${
                btn === 1 ? "btn-success" : "btn-light"
              }`}
              onClick={() => this.handleDay(1)}
            >
              {date.getDate() + 1} {monthNames[date.getMonth()]}
            </button>
            <button
              type="button"
              className={`btn btn-sm m-3 ${
                btn === 2 ? "btn-success" : "btn-light"
              }`}
              onClick={() => this.handleDay(2)}
            >
              {date.getDate() + 2} {monthNames[date.getMonth()]}
            </button>
          </div>
          <div className="col-2" style={{ borderRight: "2px solid lightgray" }}>
            <div className="dropdown1">
              <button className="dropbtn1">
                Filter Price&nbsp;&nbsp;&nbsp;
                <i className="fas fa-chevron-down"></i>
              </button>
              <div className="dropdown-content1">
                {prices.map((p) => (
                  <div>
                    <input
                      type="checkbox"
                      name="price"
                      value={p}
                      checked={price.find((val) => val === p)}
                      onChange={this.handleChange}
                    />
                    &nbsp;
                    <label>{p}</label>
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div className="col-2" style={{ borderRight: "2px solid lightgray" }}>
            <div className="dropdown1">
              <button className="dropbtn1">
                Filter ShowTime&nbsp;&nbsp;&nbsp;
                <i className="fas fa-chevron-down"></i>
              </button>
              <div className="dropdown-content1">
                {timings.map((t) => (
                  <div>
                    <input
                      type="checkbox"
                      name="time"
                      value={t}
                      checked={time.find((val) => val === t)}
                      onChange={this.handleChange}
                    />
                    &nbsp;
                    <label>{t}</label>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-9">
            <div
              className="row"
              style={{ backgroundColor: "rgb(245,191,169)", height: "65px" }}
            >
              <div className="col-6 border-right" style={{ height: "65px" }}>
                <span style={{ color: "seagreen", fontSize: "30px" }}>
                  <i class="fas fa-mobile-alt"></i>
                </span>
                <p style={{ fontSize: "12px" }}>M-Ticket Available</p>
              </div>
              <div className="col-6" style={{ height: "65px" }}>
                <span style={{ color: "orange", fontSize: "30px" }}>
                  <i class="fas fa-hamburger"></i>
                </span>
                <p style={{ fontSize: "12px" }}>Food Available</p>
              </div>
            </div>
            {movie.showTiming
              ? movie.showTiming[btn].map((m, index) => (
                  <div className="row pb-2">
                    <div className="col-1">
                      <i class="far fa-heart"></i>
                    </div>
                    <div className="col-4">
                      <div className="col-12" style={{ fontSize: "12px" }}>
                        <strong>{m.name}</strong>
                      </div>
                      <div className="col-12">
                        <span style={{ color: "seagreen", fontSize: "25px" }}>
                          <i class="fas fa-mobile-alt"></i>
                        </span>
                        <span style={{ fontSize: "10px" }}>M-Ticket</span>{" "}
                        &nbsp;&nbsp;&nbsp;
                        <span style={{ color: "orange", fontSize: "25px" }}>
                          <i class="fas fa-hamburger"></i>
                        </span>
                        <span style={{ fontSize: "10px" }}>F&B</span>
                      </div>
                    </div>
                    <div className="col-6 row">
                      {m.timings.map((b,ind) => (
                        <div key={ind}>
                          <Link
                            to={`/bookMovie/${city}/${id}/buyTicket/${index}/${ind}/${day}?time=${date}`}
                          >
                            <button
                              type="button"
                              className="btn border-dark m-1 btn-sm text-primary"
                              data-bs-toggle="tooltip"
                              data-bs-placement="bottom"
                              title={b.price}
                            >
                              {b.name}
                            </button>
                          </Link>
                        </div>
                      ))}
                      <div className="col-12">
                        <span
                          className="m-2"
                          style={{ color: "red", fontSize: "6px" }}
                        >
                          <i class="fas fa-circle"></i>
                        </span>
                        <small>Cancellation available</small>
                      </div>
                    </div>
                  </div>
                ))
              : ""}
          </div>
          <div className="col-2">
            <img
              className="img-fluid"
              src="https://i.ibb.co/JqbbCJz/1331654202504679967.jpg"
            />
          </div>
        </div>
      </div>
    );
  }
}
export default MovieSlots;
