import React, { Component } from "react";
import Logo from "./bmsLogo.svg";
import "./navBar.css";
import { Link } from "react-router-dom";
class NavBar extends Component {

  render() {
    let { user, handleCity, city } = this.props;
    return (<>
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark p-0">
        <Link className="navbar-brand text-dark ml-1 mr-5" to="/">
          <img src={Logo} />
        </Link>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav">
            <input
              className="form-control ml-4 mt-1 mr-5 searchbar"
              type="text"
              name="search"
              placeholder="&#xF002; Search"
              style={{ width: "450px", height:"33px", fontFamily: "FontAwesome" }}
            />
            <li className="nav-item ml-5">
              <div className="dropdown">
                <button className="dropbtn">{city+" "}<i className="fas fa-chevron-down"></i></button>
                <div className="dropdown-content">
                  <Link to="/home/NCR" onClick={()=>handleCity("NCR")}>NCR</Link>
                  <Link to="/home/Ahmedabad" onClick={()=>handleCity("Ahmedabad")}>Ahmedabad</Link>
                  <Link to="/home/Bangalore" onClick={()=>handleCity("Bangalore")}>Bangalore</Link>
                  <Link to="/home/Chennai" onClick={()=>handleCity("Chennai")}>Chennai</Link>
                  <Link to="/home/Mumbai" onClick={()=>handleCity("Mumbai")}>Mumbai</Link>
                  <Link to="/home/Hydrabad" onClick={()=>handleCity("Hydrabad")}>Hydrabad</Link>
                </div>
              </div>
            </li>
            <li className="nav-item ml-5 mt-2">
              <Link className="nav-link text-white" to="/">
                English
              </Link>
            </li>
            <li className="nav-item ml-5 mt-2">
              <button type="button" className="btn btn-outline-light btn-sm">
                Sign In
              </button>
            </li>
          </ul>
        </div>
      </nav>
      <nav className="navbar row navbar-expand-sm navbar-dark bg-dark p-0" style={{fontSize: "14px"}} >
            <div className="col-2 text-center text-white">Movies</div>
            <div className="col-2 text-center text-white">Events</div>
            <div className="col-2 text-center text-white">Plays</div>
            <div className="col-2 text-center text-white">Activities</div>
            <div className="col-2 text-center text-white">Fanhood</div>
      </nav>
      </>
    );
  }
}
export default NavBar;
