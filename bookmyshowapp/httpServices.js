import axios from "axios";

const baseURL = "https://us-central1-bkyow-22da6.cloudfunctions.net/app";

function get(url) {
  return axios.get(baseURL + url);
}

export default {
  get,
};
