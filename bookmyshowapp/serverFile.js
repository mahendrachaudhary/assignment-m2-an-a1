var bodyParser = require("body-parser");
var users = require("./user.js");
var cfg = require("./config.js");
var jwt = require("jsonwebtoken");
var passport = require("passport");
var passportJWT = require("passport-jwt");
var ExtractJwt = passportJWT.ExtractJwt;
var Strategy = passportJWT.Strategy;
var params = {
  secretOrKey: cfg.jwtSecret,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
};
var express = require("express");
var app = express();
app.use(express.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Credentials", "true");
  res.header(
    "Access-Control-Allow-Methods",
    "GET, POST, DELETE, HEAD, OPTIONS"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Headers, Origin, Authorization, X-Requested-With, Content-Type, Accept, Access-Control-Request"
  );
  res.setHeader("Access-Control-Expose-Headers", "*"); //add this setting line to make headers visible
  next();
});

const port = 2410;
app.listen(port, () => console.log("Listening to port :", port));
app.use(bodyParser.json());
passport.initialize();
var strategy = new Strategy(params, function (payload, done) {
  let id = users.findIndex((obj) => payload.id === obj.id);
  var user = users[+id] || null;
  if (user) {
    return done(null, {
      id: user.id,
    });
  } else {
    return done(new Error("User not found"), null);
  }
});
passport.use(strategy);

const jwtExpirySeconds = 300;

// let csvtojson = require("csvtojson");
// let request = require("request");
const con = require("./db_connection");

var users = [];
function setValue(value) {
  users = value;
}
con.query("SELECT * FROM persons", function (err, data) {
  if (err) throw err;
  setValue(data);
});
app.post("/user", function (req, res, payload) {
  if (req.body.email && req.body.password) {
    var email = req.body.email;
    var password = req.body.password;
    var user = users.find(function (u) {
      return u.email === email && u.password === password;
    });
    if (user) {
      var payload = {
        id: user.id,
      };
      var token = jwt.sign(payload, cfg.jwtSecret, {
        algorithm: "HS256",
        expiresIn: jwtExpirySeconds,
      });
      res.setHeader("X-Auth-Token", token);
      res.json({ success: true, token: "bearer " + token });
    } else {
      res.sendStatus(401);
    }
  } else {
    res.sendStatus(401);
  }
});
app.get(
  "/user",
  passport.authenticate("jwt", { session: false }),
  function (req, res) {
    res.json(users[users.findIndex((obj) => obj.id === req.user.id)]);
  }
);
var logout = require("express-passport-logout");
app.delete(
  "/logout",
  passport.authenticate("jwt", { session: false }),
  logout()
);
app.get("/allData", function (req, res) {
  con.query("SELECT * FROM lectures", function (err, data) {
    if (err) throw err;
    res.send(data);
  });
});

app.post("/createUser", function (req, res) {
  let email = req.body.email;
  let obj = users.find((item) => item.email === email);
  console.log(obj);
  if (obj === undefined) {
    const user = {
      id: users.length + 1,
      name: req.body.name,
      password: req.body.password,
      role: req.body.role,
      email: req.body.email,
    };
    users.push(user);
    res.send(user);
  } else res.status(500).send("Email already present");
});
