import React, { Component } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import authService from "../services/authService";
import AddCustomer from "./addCustomer";
import Admin from "./admin";
import AllCheques from "./allCheques";
import AllCustomers from "./allCustomers";
import AllNetbanking from "./allNetbanking";
import ChequeTran from "./chequeTran";
import Customer from "./customer";
import CustomerDetails from "./customerDetails";
import Login from "./login";
import Logout from "./logout";
import NavBar from "./navBar";
import NetTran from "./netTran";
import NomineeDetails from "./nomineeDetails";
import NotAllowed from "./notAllowed";
import PayeeName from "./payeeName";
import ViewCheque from "./viewCheque";
import ViewNet from "./viewNet";
class Home extends Component {
  render() {
    const user = authService.getUser();
    console.log(user);
    return (
      <React.Fragment>
        <NavBar user={user} />
        <Switch>
          <Route
            path="/netBanking"
            render={(props) =>
              user && user.role === "customer" ? (
                <NetTran {...props} user={user} />
              ) : (
                <NotAllowed {...props} />
              )
            }
          />
          <Route
            path="/addPayee"
            render={(props) =>
              user && user.role === "customer" ? (
                <PayeeName {...props} user={user} />
              ) : (
                <NotAllowed {...props} />
              )
            }
          />
          <Route
            path="/allNet"
            render={(props) =>
              user && user.role === "manager" ? (
                <AllNetbanking {...props} />
              ) : (
                <NotAllowed {...props} />
              )
            }
          />
          <Route
            path="/viewNet"
            render={(props) =>
              user && user.role === "customer" ? (
                <ViewNet {...props} user={user} />
              ) : (
                <NotAllowed {...props} />
              )
            }
          />
          <Route
            path="/allCheque"
            render={(props) =>
              user && user.role === "manager" ? (
                <AllCheques {...props} />
              ) : (
                <NotAllowed {...props} />
              )
            }
          />
          <Route
            path="/cheque"
            render={(props) =>
              user && user.role === "customer" ? (
                <ChequeTran {...props} user={user} />
              ) : (
                <NotAllowed {...props} />
              )
            }
          />
          <Route
            path="/viewCheque"
            render={(props) =>
              user && user.role === "customer" ? (
                <ViewCheque {...props} user={user} />
              ) : (
                <NotAllowed {...props} />
              )
            }
          />
          <Route
            path="/allCustomers"
            render={(props) =>
              user && user.role === "manager" ? (
                <AllCustomers {...props} />
              ) : (
                <NotAllowed {...props} />
              )
            }
          />
          <Route
            path="/customerDetails"
            render={(props) =>
              user && user.role === "customer" ? (
                <CustomerDetails {...props} user={user} />
              ) : (
                <NotAllowed {...props} />
              )
            }
          />
          <Route
            path="/nomineeDetails"
            render={(props) =>
              user && user.role === "customer" ? (
                <NomineeDetails {...props} user={user} />
              ) : (
                <NotAllowed {...props} />
              )
            }
          />
          <Route
            path="/addCustomer"
            render={(props) =>
              user && user.role === "manager" && <AddCustomer {...props} />
            }
          />{" "}
          <Route path="/admin" component={Admin} />
          <Route path="/customer" component={Customer} />
          <Route path="/login" component={Login} />
          <Route path="/logout" component={Logout} />
          <Redirect to="/login" from="/" />
        </Switch>
      </React.Fragment>
    );
  }
}
export default Home;
