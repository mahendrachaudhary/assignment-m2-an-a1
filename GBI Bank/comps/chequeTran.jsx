import React, {Component} from "react";
import httpService from "../services/httpService";
class ChequeTran extends Component {
    state = {
        chequeDetails: { name : "", chequeNumber: "", bankName: "", branch: "", amount: "" },
        banks: [],
        errors: {},
    }
    async componentDidMount() {
        let response = await httpService.get("/getBanks");
        let {data} = response;
        this.setState({banks: data});
    }
    handleChange = (e) => {
        const { currentTarget: input } = e;
        let s1 = { ...this.state };
        s1.chequeDetails[input.name] = input.value;
        this.handleValidate(e);
        this.setState(s1);
      };
      async postData(url,obj) {
        try {
            let response = await httpService.post(url, obj);
            let {status} = response;
            if(status===200) {
                alert("Cheque details successfully updated !!");
                this.props.history.push("/customer");
            }
        }
        catch (ex) {
            if(ex.response && ex.response.status === 401) {
                alert("Add a valid customer !!");
            }
        }
    }
    handleSubmit = (e) => {
      e.preventDefault();
      let s1 = { ...this.state };
      let errors = this.validateAll();
      if (this.isValid(errors)) {
          
          s1.chequeDetails.name = this.props.user.name;
          this.postData("/postCheque",s1.chequeDetails);
      } else {
        s1.errors = errors;
        this.setState(s1);
      }
    };
    isFormValid = () => {
      let errors = this.validateAll();
      return this.isValid(errors);
    };
    isValid = (errors) => {
      let keys = Object.keys(errors);
      let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
      return count === 0;
    };
    validateAll = () => {
      let { chequeNumber, branch } = this.state.chequeDetails;
      let errors = {};
      errors.chequeNumber = this.validateChequeNumber(chequeNumber);
      errors.branch = this.validateBranch(branch);
      return errors;
    };
    handleValidate = (e) => {
      const { currentTarget: input } = e;
      let s1 = { ...this.state };
      switch (input.name) {
        case "chequeNumber":
          s1.errors.chequeNumber = this.validateChequeNumber(input.value);
          break;
        case "branch":
          s1.errors.branch = this.validateBranch(input.value);
          break;
        default:
          break;
      }
    };
    validateChequeNumber = (chequeNumber) => chequeNumber.length > 10 ? "" : "Enter your 11 digit Cheque Number";
    validateBranch = (branch) => branch.length >3 ? "" : "Enter 4 digit code of Branch";
    render() {
        const {banks = [], errors} = this.state;
        const { chequeNumber, bankName, branch, amount } = this.state.chequeDetails;
        return <div className="container">
            <h2>Deposit Cheque</h2>
            <div className="form-group">
          <label>
            Cheque Number<b className="text-danger">*</b>
          </label>
          <input
            type="text"
            className="form-control"
            name="chequeNumber"
            value={chequeNumber}
            placeholder="Enter Cheque Number"
            onChange={this.handleChange}
          />
          {errors.chequeNumber ? (
              <div className=" col-12 text-danger" style={{backgroundColor: "bisque"}}>{errors.chequeNumber}</div>
            ) : (
              ""
            )}
        </div>
        <hr/>
        <div className="form-group">
            <label>Bank Name</label>
            <select
              className="form-control"
              name="bankName"
              value={bankName}
              onChange={this.handleChange}
            >
              <option disabled value="">
                Select The Bank
              </option>
              {banks.map((c) => (
                <option>{c}</option>
              ))}
            </select>
          </div>
        <hr/>
        <div className="form-group">
          <label>
            Branch<b className="text-danger">*</b>
          </label>
          <input
            type="text"
            className="form-control"
            name="branch"
            value={branch}
            placeholder="Enter Branch Code"
            onChange={this.handleChange}
          />
          {errors.branch ? (
              <div className=" col-12 text-danger" style={{backgroundColor: "bisque"}}>{errors.branch}</div>
            ) : (
              ""
            )}
        </div>
        <hr/>
        <div className="form-group">
          <label>
            Amount<b className="text-danger">*</b>
          </label>
          <input
            type="text"
            className="form-control"
            name="amount"
            value={amount}
            placeholder="Enter Amount"
            onChange={this.handleChange}
          />
        </div>
        <hr/>
        <button className="btn btn-primary btn-sm" onClick={this.handleSubmit}>Add Cheque</button>
        </div>
    }
}
export default ChequeTran;