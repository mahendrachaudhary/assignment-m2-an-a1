import React, { Component } from "react";
class LeftPanel extends Component {
    handleChange = (e) => {
        const {currentTarget: input} = e;
        let options = {...this.props.options};
        options[input.name] = input.value;
        this.props.onOptionChange(options);
    }
    makeRadios = (arr, values, name, label) => (
        <div className="mb-2">
          <label className="form-check-label font-weight-bold border p-2 bg-light" style={{width: "203px"}}>{label}</label>
          {arr.map((opt) => (
            <div className="form-check border p-2" key={opt}>
              <input
                className="form-check-input"
                value={opt}
                type="radio"
                name={name}
                checked={values === opt}
                onChange={this.handleChange}
                style={{marginLeft: "10px"}}
              />
              <label className="form-check-label" style={{marginLeft: "30px"}}>{opt}</label>
            </div>
          ))}
        </div>
      );
  render() {
    const { bankList, amountList} = this.props;
    let { bank="", amount="" } = this.props.options;
    return (
      <React.Fragment>
          {this.makeRadios(bankList, bank, "bank", "Bank")}
          {this.makeRadios(amountList, amount, "amount", "Amount")}
      </React.Fragment>
    );
  }
}
export default LeftPanel;
