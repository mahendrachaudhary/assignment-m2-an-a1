import React, { Component } from "react";
import httpService from "../services/httpService";
class CustomerDetails extends Component {
  state = {
    customer: {
      gender: "",
      year: "",
      month: "",
      day: "",
      PAN: "",
      addressLine1: "",
      addressLine2: "",
      state: "",
      city: "",
    },
    stateCity: {},
  };
  async postData(url, obj) {
    try {
      let { user } = this.props;
      let response = await httpService.post(url, obj);
      let { status } = response;
      if (status === 200) {
        alert(`${user.name} details added successfully !!`);
        this.props.history.push("/customer");
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 401) {
        alert("Add valid details !!");
      }
    }
  }
  handleCreate = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    let {user} = this.props;
    let data = {};
    data.name = user.name;
    data.gender = s1.customer.gender;
    data.addressLine1 = s1.customer.addressLine1;
    data.addressLine2 = s1.customer.addressLine2;
    data.state = s1.customer.state;
    data.city = s1.customer.city;
    data.dob = s1.customer.day + "-" + s1.customer.month + "-" + s1.customer.day;
    console.log(data);
    this.postData("/customerDetails", data);
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.customer[input.name] = input.value;
    this.setState(s1);
  };
  async fetchData() {
    let { user } = this.props;
    let response = await httpService.get(`/getCustomer/${user.name}`);
    let states = await httpService.get(`/statecity`);
    let s1 = { ...this.state };
    let { data } = response;
    if (data) {
      let dob1 = data.dob.split("-");
      s1.customer.gender = data.gender;
      s1.customer.year = dob1[2];
      s1.customer.month = dob1[1];
      s1.customer.day = dob1[0];
      s1.customer.PAN = data.PAN;
      s1.customer.addressLine1 = data.addressLine1;
      s1.customer.addressLine2 = data.addressLine2;
      s1.customer.state = data.state;
      s1.customer.city = data.city;
    }
    s1.stateCity = states;
    this.setState(s1);
  }
  componentDidMount() {
    this.fetchData();
  }
  componentDidUpdate(prevProps, prevState) {
    if(!prevProps === this.props) this.fetchData();
  }
  fetchYear = () => {
    const year = new Date().getFullYear();
    const years = Array.from(new Array(42), (val, index) => year - index);
    years.sort((n1, n2) => n1 - n2);
    return years;
  };
  fetchDays = (x, y) => {
    let array = [];
    while (x <= y) {
      array.push(x);
      x++;
    }
    return array;
  };
  render() {
    const {
      gender,
      year,
      month,
      day,
      PAN,
      addressLine1,
      addressLine2,
      state,
      city,
    } = this.state.customer;
    let years = this.fetchYear();
    let months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    let monthIndex = months.findIndex((m) => m === month);
    const { data = {} } = this.state.stateCity;
    let stateIndex =
      data.length > 0 ? data.findIndex((s) => s.stateName === state) : "";
    console.log(stateIndex);
    return (
      <div className="container">
        <h2>Customer Details</h2>
        <label className="form-check-label form-check-inline col-2">
          Gender<b className="text-danger">*</b>
        </label>
        <div className="form-check form-check-inline col-3">
          <input
            className="form-check-input"
            type="radio"
            name="gender"
            value="Male"
            checked={gender === "Male"}
            onChange={this.handleChange}
          />
          <label className="form-check-label">Male</label>
        </div>
        <div className="form-check form-check-inline">
          <input
            className="form-check-input"
            type="radio"
            name="gender"
            value="Female"
            checked={gender === "Female"}
            onChange={this.handleChange}
          />
          <label className="form-check-label">Female</label>
        </div>
        <hr />
        <label>
          Date of Birth<b className="text-danger">*</b>
        </label>
        <div className="row">
          <div className="form-group col-4">
            <select
              className="form-control"
              name="year"
              value={year}
              onChange={this.handleChange}
            >
              <option disabled value="">
                Select year
              </option>
              {years.map((c) => (
                <option>{c}</option>
              ))}
            </select>
          </div>
          <div className="form-group col-4">
            <select
              className="form-control"
              name="month"
              value={month}
              onChange={this.handleChange}
            >
              <option disabled value="">
                Select month
              </option>
              {months.map((c) => (
                <option>{c}</option>
              ))}
            </select>
          </div>
          <div className="form-group col-4">
            <select
              className="form-control"
              name="day"
              disabled={monthIndex === -1}
              value={day}
              onChange={this.handleChange}
            >
              <option disabled value="">
                Select day
              </option>
              {monthIndex % 2 === 0
                ? this.fetchDays(1, 31).map((m) => <option>{m}</option>)
                : monthIndex === 1
                ? this.fetchDays(1, 28).map((m) => <option>{m}</option>)
                : this.fetchDays(1, 30).map((m) => <option>{m}</option>)}
            </select>
          </div>
        </div>
        <div className="form-group">
          <label>
            PAN<b className="text-danger">*</b>
          </label>
          <input
            type="text"
            className="form-control"
            name="PAN"
            value={PAN}
            placeholder="Enter PAN"
            onChange={this.handleChange}
          />
        </div>
        <label>Address</label>
        <div className="row">
          <div className="form-group col-5">
            <input
              type="text"
              className="form-control"
              name="addressLine1"
              value={addressLine1}
              placeholder="Line1"
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group col-5">
            <input
              type="text"
              className="form-control"
              name="addressLine2"
              value={addressLine2}
              placeholder="Line2"
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div className="row">
          <div className="form-group col-6">
            <label>
              State<b className="text-danger">*</b>
            </label>
            <select
              className="form-control"
              name="state"
              value={state}
              onChange={this.handleChange}
            >
              <option disabled value="">
                Select State
              </option>
              {data.length > 0
                ? data.map((s) => <option>{s.stateName}</option>)
                : ""}
            </select>
          </div>
          <div className="form-group col-6">
            <label>
              City<b className="text-danger">*</b>
            </label>
            <select
              className="form-control"
              name="city"
              value={city}
              disabled={!state}
              onChange={this.handleChange}
            >
              <option disabled value="">
                Select City
              </option>
              {state
                ? data[stateIndex].cityArr.map((c) => <option>{c}</option>)
                : ""}
            </select>
          </div>
        </div>
        <button className="btn btn-primary btn-sm" onClick={this.handleCreate}>
          Add Details
        </button>
      </div>
    );
  }
}
export default CustomerDetails;
