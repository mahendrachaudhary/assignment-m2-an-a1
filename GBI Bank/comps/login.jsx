import React, { Component } from "react";
import httpService from "../services/httpService";
import auth from "../services/authService";
class Login extends Component {
  state = {
    loginDetail: { name: "", password: "" },
    errors: {},
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.loginDetail[input.name] = input.value;
    console.log(s1.loginDetail);
    this.handleValidate(e);
    this.setState(s1);
  };
  async login(url, obj) {
    let response = await httpService.post(url, obj);
    let { data } = response;
    console.log(response);
    auth.login(data);
    data.role === "manager"
      ? (window.location = "/admin")
      : (window.location = "/customer");
  }
  handleSubmit = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    let errors = this.validateAll();
    if (this.isValid(errors)) {
      this.login("/login", this.state.loginDetail);
    } else {
      s1.errors = errors;
      this.setState(s1);
    }
  };
  isFormValid = () => {
    let errors = this.validateAll();
    return this.isValid(errors);
  };
  isValid = (errors) => {
    let keys = Object.keys(errors);
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
    return count === 0;
  };
  validateAll = () => {
    let { name, password } = this.state.loginDetail;
    let errors = {};
    errors.name = this.validateEmail(name);
    errors.password = this.validatePassword(password);
    return errors;
  };
  handleValidate = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    switch (input.name) {
      case "name":
        s1.errors.name = this.validateEmail(input.value);
        break;
      case "password":
        s1.errors.password = this.validatePassword(input.value);
        break;
      default:
        break;
    }
  };
  validateEmail = (name) => (name ? "" : "Email is required");
  validatePassword = (password) =>
    password
      ? password.length > 6
        ? ""
        : "Password must be of 7 characters"
      : "Password is required";
  render() {
    const { errors, loginDetail } = this.state;
    let { name, password } = loginDetail;
    return (
      <div className="container-fluid text-center">
        <div className="col-3"></div>
        <div className="container col-5 mt-5">
          <h2>Welcome to GBI Bank</h2>
          <div>User Name</div>
          <div className="form-group col-12">
            <input
              className="form-control"
              type="text"
              name="name"
              value={name}
              placeholder="Enter user name"
              onChange={this.handleChange}
            />
            <span className="blockquote-footer">
              We will not share your user name with anyone else.
            </span>
            <br />
            {errors.name ? (
              <span className="text-danger">{errors.name}</span>
            ) : (
              ""
            )}
          </div>
          <div>Password</div>
          <div className="col-12 form-group">
            <input
              className="form-control"
              type="password"
              name="password"
              value={password}
              placeholder="Enter your Password"
              onChange={this.handleChange}
            />
            {errors.password ? (
              <span className="text-danger">{errors.password}</span>
            ) : (
              ""
            )}
          </div>
          <button
            className="btn btn-primary mt-1 btn-sm"
            disabled={!this.isFormValid()}
            onClick={this.handleSubmit}
          >
            Submit
          </button>
        </div>
      </div>
    );
  }
}
export default Login;
