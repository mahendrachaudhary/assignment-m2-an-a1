import React, { Component } from "react";
import { Link } from "react-router-dom";
class NavBar extends Component {
  state = {
    isOpen1: false,
    isOpen2: false,
    isOpen3: false,
    isOpen4: false,
    isOpen5: false,
  };
  toggleOpen1 = () => this.setState({ isOpen1: !this.state.isOpen1 });
  toggleOpen2 = () => this.setState({ isOpen2: !this.state.isOpen2 });
  toggleOpen3 = () => this.setState({ isOpen3: !this.state.isOpen3 });
  toggleOpen4 = () => this.setState({ isOpen4: !this.state.isOpen4 });
  toggleOpen5 = () => this.setState({ isOpen5: !this.state.isOpen5 });
  render() {
    let { user } = this.props;
    const menuClass1 = `dropdown-menu${this.state.isOpen1 ? " show" : ""}`;
    const menuClass2 = `dropdown-menu${this.state.isOpen2 ? " show" : ""}`;
    const menuClass3 = `dropdown-menu${this.state.isOpen3 ? " show" : ""}`;
    const menuClass4 = `dropdown-menu${this.state.isOpen4 ? " show" : ""}`;
    const menuClass5 = `dropdown-menu${this.state.isOpen5 ? " show" : ""}`;
    return (
      <nav className="navbar navbar-expand-sm navbar-dark bg-warning">
        <Link className="navbar-brand text-dark" to="/">
          Home
        </Link>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            {!user && (
              <li className="nav-item">
                <Link className="nav-link text-dark" to="/login">
                  Login
                </Link>
              </li>
            )}
            {user && user.role === "manager" && (
              <li className="nav-item">
                <div className="dropdown" onClick={this.toggleOpen1}>
                  <button
                    className="btn b-0 text-dark dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                  >
                    Customers
                  </button>
                  <div
                    className={menuClass1}
                    aria-labelledby="dropdownMenuButton"
                  >
                    <Link className="dropdown-item" to="/addCustomer">
                      Add Customers
                    </Link>
                    <Link className="dropdown-item" to="/allCustomers">
                      View All Customers
                    </Link>
                  </div>
                </div>
              </li>
            )}
            {user && user.role === "manager" && (
              <li className="nav-item">
                <div className="dropdown" onClick={this.toggleOpen2}>
                  <button
                    className="btn b-0 text-dark dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                  >
                    Transactions
                  </button>
                  <div
                    className={menuClass2}
                    aria-labelledby="dropdownMenuButton"
                  >
                    <Link className="dropdown-item" to="/allCheque">
                      Cheques
                    </Link>
                    <Link className="dropdown-item" to="/allNet">
                      Net Banking
                    </Link>
                  </div>
                </div>
              </li>
            )}
            {user && user.role === "customer" && (
              <li className="nav-item">
                <div className="dropdown" onClick={this.toggleOpen3}>
                  <button
                    className="btn b-0 text-dark dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                  >
                    View
                  </button>
                  <div
                    className={menuClass3}
                    aria-labelledby="dropdownMenuButton"
                  >
                    <Link className="dropdown-item" to="/viewCheque">
                      Cheques
                    </Link>
                    <Link className="dropdown-item" to="/viewNet">
                      Net Banking
                    </Link>
                  </div>
                </div>
              </li>
            )}
            {user && user.role === "customer" && (
              <li className="nav-item">
                <div className="dropdown" onClick={this.toggleOpen4}>
                  <button
                    className="btn b-0 text-dark dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                  >
                    Details
                  </button>
                  <div
                    className={menuClass4}
                    aria-labelledby="dropdownMenuButton"
                  >
                    <Link className="dropdown-item" to="/customerDetails">
                      Customer
                    </Link>
                    <Link className="dropdown-item" to="/nomineeDetails">
                      Nominee
                    </Link>
                  </div>
                </div>
              </li>
            )}
            {user && user.role === "customer" && (
              <li className="nav-item">
                <div className="dropdown" onClick={this.toggleOpen5}>
                  <button
                    className="btn b-0 text-dark dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                  >
                    Transaction
                  </button>
                  <div
                    className={menuClass5}
                    aria-labelledby="dropdownMenuButton"
                  >
                    <Link className="dropdown-item" to="/addPayee">
                      Add Payee
                    </Link>
                    <Link className="dropdown-item" to="/cheque">
                      Cheque
                    </Link>
                    <Link className="dropdown-item" to="/netBanking">
                      NetBanking
                    </Link>
                  </div>
                </div>
              </li>
            )}
          </ul>
          <ul className="navbar-nav ml-auto">
            {user && (
              <li className="nav-item">
                <Link className="nav-link text-dark">
                  Welcome {user.name}
                </Link>
              </li>
            )} 
            { user && (<li className="nav-item">
                <Link className="nav-link text-dark" to="/logout">
                  Logout
                </Link>
              </li>
            )}
          </ul>
        </div>
      </nav>
    );
  }
}
export default NavBar;
