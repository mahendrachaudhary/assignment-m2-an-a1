import React, { Component } from "react";
import httpService from "../services/httpService";
import queryString from "query-string";
import LeftPanel from "../comps/leftPanel";
class AllNetbanking extends Component {
  state = {
    data: {},
    bankList: ["SBI", "ICICI", "HDFC", "AXIS", "DBS", "GBI"],
    amountList: ["<10000", ">10000"],
  };
  async fetchData() {
    let queryParams = queryString.parse(this.props.location.search);
    let searchStr = this.makeSearchString(queryParams);
    let response = await httpService.get(
      `/getAllNetBankings?${!searchStr ? "page=1" : searchStr}`
    );
    let { data } = response;
    this.setState({ data: data });
  }
  componentDidMount() {
    this.fetchData();
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) this.fetchData();
  }
  handleOptionChange = (options) => {
    options.page = 1;
    this.callURL("/allNet", options);
  };
  handlePage = (incr) => {
    let queryParams = queryString.parse(this.props.location.search);
    let { page = "1" } = queryParams;
    let newPage = +page + incr;
    queryParams.page = newPage;
    this.callURL("/allNet", queryParams);
  };
  callURL = (url, options) => {
    let searchString = this.makeSearchString(options);
    this.props.history.push({
      pathname: url,
      search: searchString,
    });
  };
  makeSearchString = (options) => {
    let { page, bank, amount } = options;
    let searchStr = "";
    searchStr = this.addToQueryString(searchStr, "page", page);
    searchStr = this.addToQueryString(searchStr, "bank", bank);
    searchStr = this.addToQueryString(searchStr, "amount", amount);
    return searchStr;
  };
  addToQueryString = (str, paramName, paramValue) =>
    paramValue
      ? str
        ? `${str}&${paramName}=${paramValue}`
        : `${paramName}=${paramValue}`
      : str;
  render() {
    const { data = {}, bankList, amountList } = this.state;
    const { items = [], page, totalItems, totalNum } = data;
    const queryParams = queryString.parse(this.props.location.search);
    queryParams.page = 1;
    let startIndex = (page - 1) * totalItems;
    let endIndex =
      totalItems < startIndex + totalItems + 1
        ? startIndex + totalItems - 1
        : totalNum;
    console.log(data);
    return (
      <div className=" row container">
        <div className="col-12">
          <h2>All Cheque Transactions</h2>
        </div>
        <div className="col-3">
          <LeftPanel
            bankList={bankList}
            amountList={amountList}
            options={queryParams}
            onOptionChange={this.handleOptionChange}
          />
        </div>
        <div className="col-9">
          {startIndex+1} - {endIndex+1} of {totalNum}
          <div className="col-12 row border-bottom border-top p-2 bg-white">
            <div className="col-2">
              <b>Name</b>
            </div>
            <div className="col-3">
              <b>Payee Name</b>
            </div>
            <div className="col-2">
              <b>Amount</b>
            </div>
            <div className="col-3">
              <b>Bank Name</b>
            </div>
            <div className="col-2">
              <b>Comment</b>
            </div>
          </div>
          {items.map((i, index) => (
            <div
              className={
                index % 2 === 0
                  ? "col-12 row border-bottom border-top p-2 bg-light"
                  : "col-12 row border-bottom border-top p-2 bg-white"
              }
            >
              <div className="col-2">{i.name}</div>
              <div className="col-3">{i.payeeName}</div>
              <div className="col-2">{i.amount}</div>
              <div className="col-3">{i.bankName}</div>
              <div className="col-2">{i.comment}</div>
            </div>
          ))}
          <div className="row">
            <div className="col-2">
              {page > 1 ? (
                <button
                  className="btn btn-dark text-white m-1 btn-sm"
                  onClick={() => this.handlePage(-1)}
                >
                  Prev
                </button>
              ) : (
                ""
              )}
            </div>
            <div className="col-8"></div>
            <div className="col-2">
              {totalItems < totalNum ? (
                <button
                  className="btn btn-dark text-white m-1 btn-sm"
                  onClick={() => this.handlePage(1)}
                >
                  Next
                </button>
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default AllNetbanking;
