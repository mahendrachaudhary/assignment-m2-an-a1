import React, {Component} from "react";
import httpService from "../services/httpService";
class AddCustomer extends Component {
    state = {
        customer: {name: "", password: "", cpassword: ""},
        errors: {},
    }
    handleChange = (e) => {
        const { currentTarget: input } = e;
        let s1 = { ...this.state };
        s1.customer[input.name] = input.value;
        this.handleValidate(e);
        this.setState(s1);
      };
        async postData(url,obj) {
          try {
              let response = await httpService.post(url, obj);
              let {status} = response;
              if(status===200) {
                  alert("Customer has been successfully updated !!");
                  this.props.history.push("/admin");
              }
          }
          catch (ex) {
              if(ex.response && ex.response.status === 401) {
                  alert("Add a valid customer !!");
              }
          }
      }
      handleCreate = (e) => {
        e.preventDefault();
        let s1 = { ...this.state };
        let errors = this.validateAll();
        if (this.isValid(errors)) {
            let data = {};
            data.name = s1.customer.name;
            data.password = s1.customer.password;
            this.postData("/register",data);
        } else {
          s1.errors = errors;
          this.setState(s1);
        }
      };
      isFormValid = () => {
        let errors = this.validateAll();
        return this.isValid(errors);
      };
      isValid = (errors) => {
        let keys = Object.keys(errors);
        let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
        return count === 0;
      };
      validateAll = () => {
        let { name, password, cpassword } = this.state.customer;
        let errors = {};
        errors.name = this.validateName(name);
        errors.password = this.validatePassword(password);
        errors.cpassword = this.validateCPassword(cpassword, password);
        return errors;
      };
      handleValidate = (e) => {
        const { currentTarget: input } = e;
        let s1 = { ...this.state };
        switch (input.name) {
          case "name":
            s1.errors.name = this.validateName(input.value);
            break;
          case "password":
            s1.errors.password = this.validatePassword(input.value);
            break;
          case "cpassword":
            s1.errors.cpassword = this.validateCPassword(input.value, s1.customer.password);
            break;
          default:
            break;
        }
      };
      validateName = (name) => name ? "" : "Name is required";
      validatePassword = (password) => password && password.length >6 ? "" : "Password is required. Length sholud be atleast 7 chars";
      validateCPassword = (password1, password) =>
        password1
          ? password1 === password
            ? ""
            : "Passwords does not match"
          : "Password is required";
    render() {
        const { name, password, cpassword } = this.state.customer;
        const {errors} = this.state;
        return <div className="container">
            <h2>New Customer</h2>
            <div className="form-group">
              <label>Name</label>
              <input
                className="form-control"
                type="text"
                name="name"
                value={name}
                placeholder="Enter Customer Name"
                onChange={this.handleChange}
              />
              {errors.name ? (
              <span className="text-danger">{errors.name}</span>
            ) : (
              ""
            )}
            </div>
            <div className="form-group">
              <label>Password</label>
              <input
                className="form-control"
                type="password"
                name="password"
                value={password}
                placeholder="Enter password"
                onChange={this.handleChange}
              />
              {errors.password ? (
              <span className="text-danger">{errors.password}</span>
            ) : (
              ""
            )}
            </div>
            <div className="form-group">
              <label>Confirm Password</label>
              <input
                className="form-control"
                type="password"
                name="cpassword"
                value={cpassword}
                placeholder="Re-Enter Password"
                onChange={this.handleChange}
              />
              {errors.cpassword ? (
              <span className="text-danger">{errors.cpassword}</span>
            ) : (
              ""
            )}
            </div>
            <button className="btn btn-primary btn-sm" disabled={!this.isFormValid()} onClick={this.handleCreate}>Create</button>
        </div>
    }
}
export default AddCustomer;
