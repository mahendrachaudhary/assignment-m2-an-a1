import React, { Component } from "react";
import httpService from "../services/httpService";
class NetTran extends Component {
  state = {
    details: { name: "", payeeName: "", amount: "", comment: "", bankName: "" },
    data: [],
    errors: {},
  };
  async componentDidMount() {
    let { user } = this.props;
    let response = await httpService.get(`/getPayees/${user.name}`);
    let { data } = response;
    this.setState({ data: data });
  }
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.details[input.name] = input.value;
    this.handleValidate(e);
    this.setState(s1);
  };
  async postData(url, obj) {
    try {
      let response = await httpService.post(url, obj);
      let { status } = response;
      if (status === 200) {
        alert("Net banking details successfully updated !!");
        this.props.history.push("/customer");
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 401) {
        alert("Add a valid customer !!");
      }
    }
  }
  handleSubmit = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    let errors = this.validateAll();
    if (this.isValid(errors)) {
      let payee = s1.data.find((d)=>d.payeeName === s1.details.payeeName);
      s1.details.name = payee.name;
      s1.details.bankName = payee.bankName;
      this.postData("/postNet", s1.details);
    } else {
      s1.errors = errors;
      this.setState(s1);
    }
  };
  isFormValid = () => {
    let errors = this.validateAll();
    return this.isValid(errors);
  };
  isValid = (errors) => {
    let keys = Object.keys(errors);
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
    return count === 0;
  };
  validateAll = () => {
    let { payeeName} = this.state.details;
    let errors = {};
    errors.payeeName = this.validatePayeeName(payeeName);
    return errors;
  };
  handleValidate = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    switch (input.name) {
      case "payeeName":
        s1.errors.payeeName = this.validatePayeeName(input.value);
        break;
      default:
        break;
    }
  };
  validatePayeeName = (payeeName) =>
    payeeName ? "" : "Choose Payee Name";
  render() {
    const { data = [], errors } = this.state;
    const { payeeName, amount, comment } = this.state.details;
    return (
      <div className="container">
        <h2>Net Banking Details</h2>
        <div className="form-group">
          <label>Payee Name</label>
          <select
            className="form-control"
            name="payeeName"
            value={payeeName}
            onChange={this.handleChange}
          >
            <option disabled value="">
              Select The Bank
            </option>
            {data.map((c) => (
              <option>{c.payeeName}</option>
            ))}
          </select>
          {errors.payeeName ? (
              <span className="text-danger">{errors.payeeName}</span>
            ) : (
              ""
            )}
        </div>
        <div className="form-group">
          <label>
            Account Number<b className="text-danger">*</b>
          </label>
          <input
            type="text"
            className="form-control"
            name="amount"
            value={amount}
            placeholder="Enter payee amount number"
            onChange={this.handleChange}
          />
        </div>
        <hr />
        <div className="form-group">
          <label>
            Comment<b className="text-danger">*</b>
          </label>
          <input
            type="text"
            className="form-control"
            name="comment"
            value={comment}
            placeholder="Enter Comment"
            onChange={this.handleChange}
          />
        </div>
        <button className="btn btn-primary btn-sm" onClick={this.handleSubmit} disabled={!this.isFormValid()}>
          Add Transaction
        </button>
      </div>
    );
  }
}
export default NetTran;
