import React, { Component } from "react";
import httpService from "../services/httpService";
class NomineeDetails extends Component {
  state = {
    nominee: {
      gender: "",
      year: "",
      month: "",
      daya: "",
      nomineeName: "",
      relationship: "",
      jointSignatory: "",
    },
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    if(input.type === "checkbox") {
      s1.nominee[input.name] = input.checked;
    }
    else s1.nominee[input.name] = input.value;
    this.setState(s1);
  };
  async postData(url, obj) {
    try {
      let { user } = this.props;
      let response = await httpService.post(url, obj);
      let { status } = response;
      if (status === 200) {
        alert(`${user.name} Your nominee :: ${this.state.nominee.nomineeName}`);
        this.props.history.push("/customer");
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 401) {
        alert("Add valid details !!");
      }
    }
  }
  hanldeDetails = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    let {user} = this.props;
    let data = {};
    data.name = user.name;
    data.gender = s1.nominee.gender;
    data.nomineeName = s1.nominee.nomineeName;
    data.relationship = s1.nominee.relationship;
    data.jointSignatory = s1.nominee.jointSignatory;
    data.dob = s1.nominee.day + "-" + s1.nominee.month + "-" + s1.nominee.year;
    console.log(data);
    this.postData("/nomineeDetails", data);
  };
  async componentDidMount() {
    let { user } = this.props;
    let response = await httpService.get(`/getNominee/${user.name}`);
    let { data } = response;
    if (data) {
      let dob1 = data.dob.split("-");
      let s1 = { ...this.state };
      s1.nominee.gender = data.gender;
      s1.nominee.year = dob1[2];
      s1.nominee.month = dob1[1];
      s1.nominee.day = dob1[0];
      s1.nominee.nomineeName = data.nomineeName;
      s1.nominee.relationship = data.relationship;
      s1.nominee.jointSignatory = data.jointSignatory;
      this.setState(s1);
    }
  }
  fetchYear = () => {
    const year = new Date().getFullYear();
    const years = Array.from(new Array(42), (val, index) => year - index);
    years.sort((n1, n2) => n1 - n2);
    return years;
  };
  fetchDays = (x, y) => {
    let array = [];
    while (x <= y) {
      array.push(x);
      x++;
    }
    return array;
  };
  render() {
    const {
      gender,
      year,
      month,
      day,
      nomineeName,
      jointSignatory,
      relationship,
    } = this.state.nominee;
    let years = this.fetchYear();
    let months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    let monthIndex = months.findIndex((m) => m === month);
    return (
      <div className="container">
        <h2>Nominee Details</h2>
        <div className="form-group">
          <label>
            Name<b className="text-danger">*</b>
          </label>
          <input
            type="text"
            className="form-control"
            name="nomineeName"
            value={nomineeName}
            placeholder="Enter Name"
            onChange={this.handleChange}
          />
        </div>
        <label className="form-check-label form-check-inline col-2">
          Gender<b className="text-danger">*</b>
        </label>
        <div className="form-check form-check-inline col-3">
          <input
            className="form-check-input"
            type="radio"
            name="gender"
            value="Male"
            checked={gender === "Male"}
            onChange={this.handleChange}
          />
          <label className="form-check-label">Male</label>
        </div>
        <div className="form-check form-check-inline">
          <input
            className="form-check-input"
            type="radio"
            name="gender"
            value="Female"
            checked={gender === "Female"}
            onChange={this.handleChange}
          />
          <label className="form-check-label">Female</label>
        </div>
        <hr />
        <label>
          Date of Birth<b className="text-danger">*</b>
        </label>
        <div className="row">
          <div className="form-group col-4">
            <select
              className="form-control"
              name="year"
              value={year}
              onChange={this.handleChange}
            >
              <option disabled value="">
                Select year
              </option>
              {years.map((c) => (
                <option>{c}</option>
              ))}
            </select>
          </div>
          <div className="form-group col-4">
            <select
              className="form-control"
              name="month"
              value={month}
              onChange={this.handleChange}
            >
              <option disabled value="">
                Select month
              </option>
              {months.map((c) => (
                <option>{c}</option>
              ))}
            </select>
          </div>
          <div className="form-group col-4">
            <select
              className="form-control"
              name="day"
              disabled={monthIndex === -1}
              value={day}
              onChange={this.handleChange}
            >
              <option disabled value="">
                Select day
              </option>
              {monthIndex % 2 === 0
                ? this.fetchDays(1, 31).map((m) => <option>{m}</option>)
                : monthIndex === 1
                ? this.fetchDays(1, 28).map((m) => <option>{m}</option>)
                : this.fetchDays(1, 30).map((m) => <option>{m}</option>)}
            </select>
          </div>
        </div>
        <div className="form-group">
          <label>
            Relationship<b className="text-danger">*</b>
          </label>
          <input
            type="text"
            className="form-control"
            name="relationship"
            value={relationship}
            placeholder="Enter Relationship"
            onChange={this.handleChange}
          />
        </div>
        <div className="form-check">
          <input
            type="checkbox"
            className="form-check-input"
            name="jointSignatory"
            value={jointSignatory}
            checked={jointSignatory}
            onChange={this.handleChange}
          />
          <label className="form-check-label">Joint Signatory</label>
        </div>
        <button className="btn btn-primary btn-sm" onClick={this.hanldeDetails}>Add Nominee</button>
      </div>
    );
  }
}
export default NomineeDetails;
