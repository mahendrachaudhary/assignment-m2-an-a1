import React, { Component } from "react";
import httpService from "../services/httpService";
import queryString from "query-string";
class ViewNet extends Component {
  state = {
    data: {},
  };
  async fetchData() {
    let { user } = this.props;

    let queryParams = queryString.parse(this.props.location.search);
    let searchStr = this.makeSearchString(queryParams);
    let response = await httpService.get(
      `/getNetBankingByName/${user.name}?${!searchStr ? "page=1" : searchStr}`
    );
    console.log(response);
    let { data } = response;
    this.setState({ data: data });
  }
  componentDidMount() {
    this.fetchData();
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) this.fetchData();
  }
  handlePage = (incr) => {
    let queryParams = queryString.parse(this.props.location.search);
    let { page = "1" } = queryParams;
    let newPage = +page + incr;
    queryParams.page = newPage;
    this.callURL("/viewNet", queryParams);
  };
  callURL = (url, options) => {
    let searchString = this.makeSearchString(options);
    this.props.history.push({
      pathname: url,
      search: searchString,
    });
  };
  makeSearchString = (options) => {
    let { page } = options;
    let searchStr = "";
    searchStr = this.addToQueryString(searchStr, "page", page);
    return searchStr;
  };
  addToQueryString = (str, paramName, paramValue) =>
    paramValue
      ? str
        ? `${str}&${paramName}=${paramValue}`
        : `${paramName}=${paramValue}`
      : str;
  render() {
    const { data = {} } = this.state;
    const { items = [], page, totalItems, totalNum } = data;
    let startIndex = (page - 1) * totalItems;
    let endIndex =
      totalItems < startIndex + totalItems + 1
        ? startIndex + totalItems - 1
        : totalNum;
    return (
      <div className="container">
        <h2>All Net Banking Details</h2>
        {totalItems === 0 ? <h3 className="text-danger">No Transactions to show</h3>
        :<div>{startIndex+1} - {endIndex+1} of {totalNum}
        <div className="col-12 row border-bottom border-top p-2 bg-white">
          <div className="col-3">
            <b>Payee Name</b>
          </div>
          <div className="col-3">
            <b>Amount</b>
          </div>
          <div className="col-3">
            <b>Bank</b>
          </div>
          <div className="col-3">
            <b>Comment</b>
          </div>
        </div>
        {items.map((i, index) => (
          <div
            className={
              index % 2 === 0
                ? "col-12 row border-bottom border-top p-2 bg-light"
                : "col-12 row border-bottom border-top p-2 bg-white"
            }
          >
            <div className="col-3">{i.payeeName}</div>
            <div className="col-3">{i.amount}</div>
            <div className="col-3">{i.bankName}</div>
            <div className="col-3">{i.comment}</div>
          </div>
        ))}
        <div className="row">
          <div className="col-2">
            {page > 1 ? (
              <button
                className="btn btn-dark text-white m-1 btn-sm"
                onClick={() => this.handlePage(-1)}
              >
                Prev
              </button>
            ) : (
              ""
            )}
          </div>
          <div className="col-8"></div>
          <div className="col-2">
            {totalItems < totalNum ? (
              <button
                className="btn btn-dark text-white m-1 btn-sm"
                onClick={() => this.handlePage(1)}
              >
                Next
              </button>
            ) : (
              ""
            )}
          </div>
        </div> </div>}
      </div>
    );
  }
}
export default ViewNet;
