import React, {Component} from "react";
import httpService from "../services/httpService";
class PayeeName extends Component {
    state = {
        payee: {name: "", payeeName: "", IFSC: "", accNumber: "", bankName: ""},
    }
    handleChange = (e) => {
        const { currentTarget: input } = e;
        let s1 = { ...this.state };
        s1.payee[input.name] = input.value;
        this.setState(s1);
      };
    async postData(url,obj) {
        try {
            let response = await httpService.post(url, obj);
            let {status} = response;
            if(status===200) {
                alert("Payee Name has been successfully updated !!");
                this.props.history.push("/customer");
            }
        }
        catch (ex) {
            if(ex.response && ex.response.status === 401) {
                alert("Add a valid customer !!");
            }
        }
    }
    handlePayee = (e) => {
        e.preventDefault();
        let s1 = { ...this.state };
        let payee1 = {name: "", payeeName: "", IFSC: "", accNumber: "", bankName: ""};
            payee1.name = this.props.user.name;
            payee1.payeeName = s1.payee.payeeName;
            payee1.accNumber = s1.payee.accNumber;
            payee1.bankName = s1.payee.bankName === "sameBank" ? "GBI" : "";
            this.postData("/addPayee",payee1);
       };
    render() {
        const {payeeName, accNumber, bankName} = this.state.payee
        return <div className="container">
            <h2>Add Payee</h2>
            <div className="form-group">
          <label>
            Payee Name<b className="text-danger">*</b>
          </label>
          <input
            type="text"
            className="form-control"
            name="payeeName"
            value={payeeName}
            placeholder="Enter Payee Name"
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <label>
            Account Number<b className="text-danger">*</b>
          </label>
          <input
            type="text"
            className="form-control"
            name="accNumber"
            value={accNumber}
            placeholder="Enter Account Number"
            onChange={this.handleChange}
          />
        </div>
        <div className="form-check">
          <input
            className="form-check-input"
            type="radio"
            name="bankName"
            value="sameBank"
              checked={bankName === "sameBank"}
            onChange={this.handleChange}
          />
          <label className="form-check-label">Same Bank</label>
        </div>
        <div className="form-check">
          <input
            className="form-check-input"
            type="radio"
            name="bankName"
            value="otherBank"
              checked={bankName === "otherBank"}
            onChange={this.handleChange}
          />
          <label className="form-check-label">Other Bank</label>
        </div>
        <button className="btn btn-primary btn-sm" onClick={this.handlePayee}>Add Payee</button>
        </div>
    }
}
export default PayeeName;