import React, { Component } from "react";
class LeftPanel extends Component {
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let option = { ...this.props.course };
    console.log(option);
    option[input.name] = this.updateCBs(
      option[input.name],
      input.checked,
      input.value
    );
    this.props.onOptionChange(option);
  };
  updateCBs = (inpValue, checked, value) => {
    let inpArr = inpValue ? inpValue.split(",") : [];
    if (checked) inpArr.push(value);
    else {
      let index = inpArr.findIndex((ele) => ele === value);
      if (index >= 0) inpArr.splice(index, 1);
    }
    return inpArr.join(",");
  };
  makeCheckBoxes = (label, arr, name, selArr) => {
    return (
      <React.Fragment>
        <label className="form-check-label border p-1 bg-light w-100 font-weight-bold">
          {label}
        </label>
        {arr.map((opt) => (
          <div className="form-check border p-1">
            <input
              className="form-check-input"
              type="checkbox"
              name={name}
              value={opt}
              checked={selArr.findIndex((sel) => sel === opt) >= 0}
              onChange={this.handleChange}
              style={{ marginLeft: "10px" }}
            />
            <label className="form-check-label" style={{ marginLeft: "30px" }}>
              {opt}
            </label>
          </div>
        ))}
      </React.Fragment>
    );
  };
  render() {
    const { options } = this.props;
    let { course = "" } = this.props.course;
    return (
      <React.Fragment>
        {this.makeCheckBoxes("Options", options, "course", course.split(","))}
      </React.Fragment>
    );
  }
}
export default LeftPanel;
