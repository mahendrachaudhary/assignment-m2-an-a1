import React, { Component } from "react";
import httpServices from "../services/httpServices";
class AllClasses extends Component {
  state = {
    data: [],
  };
  async componentDidMount() {
    let { user } = this.props;
    console.log(user);
    let response = await httpServices.get(`/getStudentClass/${user.name}`);
    let { data } = response;
    this.setState({ data: data });
  }
  render() {
    const { data = {} } = this.state;
    console.log(data);
    return (
      <div className="container">
        <br />
        <h4>All Scheduled Classes</h4>
        <div>
          <div className="row bg-light p-2">
            <div className="col-3">Course Name</div>
            <div className="col-2">Start Time</div>
            <div className="col-2">End Time</div>
            <div className="col-2">Faculty Name</div>
            <div className="col-3">Topic</div>
          </div>
          {data.length &&
            data.map((i, index) => (
              <div
                className="row border-bottom p-2"
                style={{ backgroundColor: "burlywood" }}
                key={index}
              >
                <div className="col-3">{i.course}</div>
                <div className="col-2">{i.time}</div>
                <div className="col-2">{i.endTime}</div>
                <div className="col-2">{i.facultyName}</div>
                <div className="col-3">{i.topic}</div>
              </div>
            ))}
        </div>
      </div>
    );
  }
}
export default AllClasses;
