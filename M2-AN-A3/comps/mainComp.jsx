import React, { Component } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import authService from "../services/authServices";
import Admin from "./admin";
import AllClasses from "./allClasses";
import AllFaculties from "./allFaculties";
import AllStudents from "./allStudents";
import CoursesAssigned from "./coursesAssigned";
import CourseStudent from "./courseStudent";
import Faculty from "./faculty";
import FacultyCourse from "./facultyCourse";
import Login from "./login";
import Logout from "./logout";
import NavBar from "./navBar";
import RegisterSF from "./registerSF";
import ScheduleClass from "./scheduleClass";
import ScheduledClasses from "./scheduledClasses";
import Student from "./student";
import StudentCourse from "./studentCourse";
import StudentDetails from "./studentDetails";

class Home extends Component {
  render() {
    const user = authService.getUser();
    console.log(user);
    return (
      <React.Fragment>
          <NavBar user={user} />
        <Switch>
          <Route path="/scheduleClass" render={(props)=> <ScheduleClass {...props} user={user} />} />
          <Route path="/scheduledClasses" render={(props)=> <ScheduledClasses {...props} user={user} />} />
          <Route path="/coursesAssigned" render={(props)=> <CoursesAssigned {...props} user={user} />} />
          <Route path="/studentDetails" render={(props)=> <StudentDetails {...props} user={user} />} />
          <Route path="/allClasses" render={(props)=> <AllClasses {...props} user={user} />} />
          <Route path="/courseStudent" render={(props)=> <CourseStudent {...props} user={user} />} />
          <Route path="/studentCourse" component={StudentCourse} />
          <Route path="/facultyCourse" component={FacultyCourse} />
          <Route path="/allStudents" component={AllStudents} />
          <Route path="/allFaculties" component={AllFaculties} />
          <Route path="/register" component={RegisterSF} />
          <Route path="/student" component={Student} />
          <Route path="/faculty" component={Faculty} />
          <Route path="/admin" component={Admin} />
          <Route path="/login" component={Login} />
          <Route path="/logout" component={Logout} />
          <Redirect to="/login" from="/" />
        </Switch>
      </React.Fragment>
    );
  }
}
export default Home;
