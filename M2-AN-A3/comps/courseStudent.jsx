import React, { Component } from "react";
import httpServices from "../services/httpServices";
class CourseStudent extends Component {
  state = {
    data: [],
  };
  async componentDidMount() {
    let { user } = this.props;
    let response = await httpServices.get(`/getStudentCourse/${user.name}`);
    let { data } = response;
    this.setState({ data: data });
  }
  render() {
    const { data = {} } = this.state;
    return (
      <div className="container">
        <br />
        <h4>Courses Assigned</h4>
        <div>
          <div className="row bg-light p-2">
            <div className="col-2">CourseId</div>
            <div className="col-3">Course Name</div>
            <div className="col-3">Course Code</div>
            <div className="col-4">Description</div>
          </div>
          {data.length &&
            data.map((i, index) => (
              <div
                className="row border-bottom p-2"
                style={{ backgroundColor: "darkseagreen" }}
                key={index}
              >
                <div className="col-2">{i.courseId}</div>
                <div className="col-3">{i.name}</div>
                <div className="col-3">{i.code}</div>
                <div className="col-4">{i.description}</div>
              </div>
            ))}
        </div>
      </div>
    );
  }
}
export default CourseStudent;
