import React, { Component } from "react";
import httpServices from "../services/httpServices";
class StudentDetails extends Component {
  state = {
    student: { name: "", year: "", month: "", day: "", gender: "", about: "" },
    setDetails: true,
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.student[input.name] = input.value;
    this.setState(s1);
  };
  async componentDidMount() {
    try {
      let { user } = this.props;
      let response = await httpServices.get(`/getStudentDetails/${user.name}`);
      let { data } = response;
      console.log(data);
      if (data.about) {
        let s1 = { ...this.state };
        s1.setDetails = false;
        let dob1 = data.dob.split("-");
        s1.student.gender = data.gender;
        s1.student.year = dob1[2];
        s1.student.month = dob1[1];
        s1.student.day = dob1[0];
        s1.student.name = data.name;
        s1.student.about = data.about;
        this.setState(s1);
      }
    } catch (ex) {
      console.log(ex);
    }
  }
  async postData(url, obj) {
    let response = await httpServices.post(url, obj);
    let {data} = response;
    alert(JSON.stringify(data)+" added !!");
    this.props.history.push("/student");
  }
  handleSubmit = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    let { user } = this.props;
    let data = {};
    data.name = user.name;
    data.gender = s1.student.gender;
    data.about = s1.student.about;
    data.dob = s1.student.day + "-" + s1.student.month + "-" + s1.student.year;
    this.postData("/postStudentDetails", data);
  };
  fetchYear = () => {
    const year = new Date().getFullYear();
    const years = Array.from(new Array(42), (val, index) => year - index);
    years.sort((n1, n2) => n1 - n2);
    return years;
  };
  fetchDays = (x, y) => {
    let array = [];
    while (x <= y) {
      array.push(x);
      x++;
    }
    return array;
  };
  render() {
    let { year, month, day, gender, about } = this.state.student;
    let years = this.fetchYear();
    let months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    let monthIndex = months.findIndex((m) => m === month);
    return (
      <div className="container">
        <h1>Student details</h1>
        <label className="form-check-label col-2">
          Gender<b className="text-danger">*</b>
        </label>
        <div className="form-check form-check-inline">
          <input
            className="form-check-input"
            type="radio"
            name="gender"
            value="male"
            checked={gender === "male"}
            onChange={this.handleChange}
          />
          <label className="form-check-label">Male</label>
        </div>
        <div className="form-check form-check-inline">
          <input
            className="form-check-input"
            type="radio"
            name="gender"
            value="female"
            checked={gender === "female"}
            onChange={this.handleChange}
          />
          <label className="form-check-label">Female</label>
        </div>
        <br />
        <hr />
        <label>
          Date of Birth<b className="text-danger">*</b>
        </label>
        <div className="row">
          <div className="form-group col-4">
            <select
              className="form-control"
              name="year"
              value={year}
              onChange={this.handleChange}
            >
              <option disabled value="">
                Select year
              </option>
              {years.map((c) => (
                <option>{c}</option>
              ))}
            </select>
          </div>
          <div className="form-group col-4">
            <select
              className="form-control"
              name="month"
              value={month}
              onChange={this.handleChange}
            >
              <option disabled value="">
                Select month
              </option>
              {months.map((c) => (
                <option>{c}</option>
              ))}
            </select>
          </div>
          <div className="form-group col-4">
            <select
              className="form-control"
              name="day"
              disabled={monthIndex === -1}
              value={day}
              onChange={this.handleChange}
            >
              <option disabled value="">
                Select day
              </option>
              {monthIndex % 2 === 0
                ? this.fetchDays(1, 31).map((m) => <option>{m}</option>)
                : monthIndex === 1
                ? this.fetchDays(1, 28).map((m) => <option>{m}</option>)
                : this.fetchDays(1, 30).map((m) => <option>{m}</option>)}
            </select>
          </div>
        </div>
        <br />
        <div className="form-group">
          <label>About Myself</label>
          <textarea
            className="form-control"
            type="text"
            rows="5"
            name="about"
            value={about}
            onChange={this.handleChange}
          />
        </div>
        {this.state.setDetails ? (
          <button
            className="btn btn-primary btn-sm"
            onClick={this.handleSubmit}
          >
            Add Details
          </button>
        ) : ""}
      </div>
    );
  }
}
export default StudentDetails;
