import React, { Component } from "react";
import httpServices from "../services/httpServices";
class ScheduleClass extends Component {
  state = {
    class: { course: "", time: "", endTime: "", topic: "" },
    data: {},
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.class[input.name] = input.value;
    this.setState(s1);
  };
  async componentDidMount() {
    let { user } = this.props;
    let response = await httpServices.get(`/getFacultyClass/${user.name}`);
    let { data } = response;
    this.setState({ data: data });
  }
  async postData(url, obj) {
    let response = await httpServices.post(url, obj);
    this.props.history.push("/scheduledClasses");
  }
  handleSchedule = (e) => {
    e.preventDefault();
    let { user } = this.props;
    let data = { ...this.state.class, facultyName: user.name };
    this.postData("/postClass", data);
  };
  render() {
    const { course, time, endTime, topic } = this.state.class;
    const { data = {} } = this.state;
    return (
      <div className="container">
        <br />
        <h3>Schedule a class</h3>
        <div className="form-group">
          <select
            className="form-control"
            name="course"
            value={course}
            onChange={this.handleChange}
          >
            <option disabled value="">
              Select the Course
            </option>
            {data.length && data.map((c) => <option>{c.course}</option>)}
          </select>
        </div>
        <div className="form-group">
          <label>
            Start Time<b className="text-danger">*</b>
          </label>
          <input
            className="form-control"
            type="time"
            name="time"
            value={time}
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <label>
            End Time<b className="text-danger">*</b>
          </label>
          <input
            className="form-control"
            type="time"
            name="endTime"
            value={endTime}
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <label>
            Topic<b className="text-danger">*</b>
          </label>
          <input
            className="form-control"
            type="text"
            name="topic"
            value={topic}
            onChange={this.handleChange}
          />
        </div>
        <button
          className="btn btn-primary btn-sm mt-2"
          onClick={this.handleSchedule}
        >
          Schedule
        </button>
      </div>
    );
  }
}
export default ScheduleClass;
