import React, { Component } from "react";
import httpServices from "../services/httpServices";
class StudentCourse extends Component {
  state = {
    data: {},
    edit: -1,
    course: {},
    studentNames: [],
  };
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    if (input.type === "checkbox") {
      s1.course[input.name] = this.updateCBs(
        s1.course[input.name],
        input.checked,
        input.value
      );
    } else {
      s1.course[input.name] = input.value;
    }
    this.setState(s1);
  };
  updateCBs = (inpValue, checked, value) => {
    let inpArr = inpValue ? inpValue : [];
    if (checked) inpArr.push(value);
    else {
      let index = inpArr.findIndex((ele) => ele === value);
      if (index >= 0) inpArr.splice(index, 1);
    }
    return inpArr;
  };
  async componentDidMount() {
    let response = await httpServices.get("/getCourses");
    let response1 = await httpServices.get("/getStudentNames");
    let { data } = response;
    let studentNames = response1.data;
    this.setState({ data: data, studentNames: studentNames });
  }
  async putData(url, obj) {
    let response = await httpServices.put(url, obj);
    if (response) {
      this.setState({ edit: -1 });
    }
  }
  handleUpdate = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    this.putData("/putCourse", s1.course);
  };
  handleEdit = (index) => {
    let s1 = { ...this.state };
    s1.course = s1.data[index];
    s1.edit = index;
    this.setState(s1);
  };
  render() {
    const { data = {}, edit, studentNames = [] } = this.state;
    const { name, description, code, students } = this.state.course;
    return (
      <div className="container">
        <br />
        <h4>Add students to a course</h4>
        {edit === -1 ? (
          <div>
            <div className="row bg-light">
              <div className="col-1">CourseId</div>
              <div className="col-3">Name</div>
              <div className="col-2">Course Code</div>
              <div className="col-3">Description</div>
              <div className="col-2">Students</div>
              <div className="col-1"></div>
            </div>
            {data.length &&
              data.map((i, index) => (
                <div
                  className="row border-bottom boder-white bg-warning"
                  key={index}
                >
                  <div className="col-1">{i.courseId}</div>
                  <div className="col-3">{i.name}</div>
                  <div className="col-2">{i.code}</div>
                  <div className="col-3">{i.description}</div>
                  <div className="col-2">
                    {i.students.map((s) => (
                      <div>{s}</div>
                    ))}
                  </div>
                  <div className="col-1">
                    <button
                      className="btn btn-dark btn-sm m-1"
                      onClick={() => this.handleEdit(index)}
                    >
                      Edit
                    </button>
                  </div>
                </div>
              ))}
          </div>
        ) : (
          <div>
            <hr />
            <h3>Edit the Course</h3>
            <div className="form-group">
              <label>Name</label>
              <input
                className="form-control"
                type="text"
                name="name"
                value={name}
                onChange={this.handleChange}
              />
            </div>
            <div className="form-group">
              <label>Course Code</label>
              <input
                className="form-control"
                type="text"
                name="code"
                value={code}
                onChange={this.handleChange}
              />
            </div>
            <div className="form-group">
              <label>Description</label>
              <input
                className="form-control"
                type="text"
                name="description"
                value={description}
                onChange={this.handleChange}
              />
            </div>
            <label className="form-check-label">
              Students<b className="text-danger">*</b>
            </label>
            {studentNames.map((opt) => (
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="checkbox"
                  name="students"
                  value={opt}
                  checked={students.findIndex((sel) => sel === opt) >= 0}
                  onChange={this.handleChange}
                />
                <label className="form-check-label">{opt}</label>
              </div>
            ))}
            <button
              className="btn btn-primary btn-sm"
              onClick={this.handleUpdate}
            >
              Update
            </button>
          </div>
        )}
      </div>
    );
  }
}
export default StudentCourse;
