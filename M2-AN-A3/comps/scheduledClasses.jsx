import React, { Component } from "react";
import httpServices from "../services/httpServices";
class ScheduledClasses extends Component {
  state = {
    data: {},
    edit: -1,
    class: {},
  };
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
      s1.class[input.name] = input.value;
    this.setState(s1);
  };
  updateCBs = (inpValue, checked, value) => {
    let inpArr = inpValue ? inpValue : [];
    if (checked) inpArr.push(value);
    else {
      let index = inpArr.findIndex((ele) => ele === value);
      if (index >= 0) inpArr.splice(index, 1);
    }
    return inpArr;
  };
  async componentDidMount() {
      let {user} = this.props;
    let response = await httpServices.get(`/getFacultyClass/${user.name}`);
    let { data } = response;
    this.setState({ data: data});
  }
  async putData(url, obj) {
    let response = await httpServices.put(url, obj);
    if (response) {
        alert("Class Updated ::"+JSON.stringify(response.data))
      this.setState({ edit: -1 });
    }
  }
  handleSchedule = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    
    this.putData(`/postClass/${s1.class.classId}`, s1.class);
  };
  handleEdit = (index) => {
    let s1 = { ...this.state };
    s1.class = s1.data[index];
    s1.edit = index;
    this.setState(s1);
  };
  handleSNewClass = (e) => {
    e.preventDefault();
    window.location = "/scheduleClass";
  }
  render() {
    const { data = {}, edit } = this.state;
    const { course, time, endTime, topic } = this.state.class;
    console.log(data);
    return (
      <div className="container">
        <br />
        
        {edit === -1 ? (
          <div>
              <h4>All Scheduled Classes</h4>
            <div className="row bg-light">
              <div className="col-3">Course Name</div>
              <div className="col-2">Start Time</div>
              <div className="col-2">End Time</div>
              <div className="col-2">Topic</div>
              <div className="col-3"></div>
            </div>
            {data.length &&
              data.map((i, index) => (
                <div
                  className="row border-bottom"
                  style={{backgroundColor: "bisque"}}
                  key={index}
                >
                  <div className="col-3">{i.course}</div>
                  <div className="col-2">{i.time}</div>
                  <div className="col-2">{i.endTime}</div>
                  <div className="col-2">{i.topic}</div>
                  <div className="col-3">
                    <button
                      className="btn btn-dark btn-sm m-1"
                      onClick={() => this.handleEdit(index)}
                    >
                      Edit
                    </button>
                  </div>
                </div>
              ))}
              <button className="btn btn-primary btn-sm mt-2"onClick={this.handleSNewClass}>Add New Class</button>
          </div>
        ) : (
          <div>
            <h3>Edit Details of class</h3>
            <div className="form-group">
          <select
            className="form-control"
            name="course"
            value={course}
            onChange={this.handleChange}
          >
            <option disabled value="">
              Select the Course
            </option>
            {data.length && data.map((c) => <option>{c.course}</option>)}
          </select>
        </div>
        <div className="form-group">
          <label>
            Start Time<b className="text-danger">*</b>
          </label>
          <input
            className="form-control"
            type="time"
            name="time"
            value={time}
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <label>
            End Time<b className="text-danger">*</b>
          </label>
          <input
            className="form-control"
            type="time"
            name="endTime"
            value={endTime}
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <label>
            Topic<b className="text-danger">*</b>
          </label>
          <input
            className="form-control"
            type="text"
            name="topic"
            value={topic}
            onChange={this.handleChange}
          />
        </div>
        <button
          className="btn btn-primary btn-sm mt-2"
          onClick={this.handleSchedule}
        >
          Schedule
        </button>
          </div>
        )}
      </div>
    );
  }
}
export default ScheduledClasses;
