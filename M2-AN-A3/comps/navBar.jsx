import React, { Component } from "react";
import { Link } from "react-router-dom";
class NavBar extends Component {
  state = {
    isOpen1: false,
    isOpen2: false,
    isOpen3: false,
    isOpen4: false,
    isOpen5: false,
  };
  toggleOpen1 = () => this.setState({ isOpen1: !this.state.isOpen1 });
  toggleOpen2 = () => this.setState({ isOpen2: !this.state.isOpen2 });
  toggleOpen3 = () => this.setState({ isOpen3: !this.state.isOpen3 });
  toggleOpen4 = () => this.setState({ isOpen4: !this.state.isOpen4 });
  toggleOpen5 = () => this.setState({ isOpen5: !this.state.isOpen5 });
  render() {
    let { user } = this.props;
    const menuClass1 = `dropdown-menu${this.state.isOpen1 ? " show" : ""}`;
    const menuClass2 = `dropdown-menu${this.state.isOpen2 ? " show" : ""}`;
    const menuClass3 = `dropdown-menu${this.state.isOpen3 ? " show" : ""}`;
    const menuClass4 = `dropdown-menu${this.state.isOpen4 ? " show" : ""}`;
    const menuClass5 = `dropdown-menu${this.state.isOpen5 ? " show" : ""}`;
    return (
      <nav className="navbar navbar-expand-sm navbar-dark bg-success">
        <Link className="navbar-brand text-dark" to="/">
          <b>
            {user && user.role === "student"
              ? "Student Home"
              : user && user.role === "faculty"
              ? "Faculty Home"
              : "Home"}
          </b>
        </Link>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            {user && user.role === "admin" && (
              <li className="nav-item">
                <Link className="nav-link text-dark" to="/register">
                  Register
                </Link>
              </li>
            )}
            {user && user.role === "admin" && (
              <li className="nav-item">
                <div className="dropdown" onClick={this.toggleOpen1}>
                  <button
                    className="btn b-0 text-dark dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                  >
                    Assign
                  </button>
                  <div
                    className={menuClass1}
                    aria-labelledby="dropdownMenuButton"
                  >
                    <Link className="dropdown-item" to="/studentCourse">
                      Student to Course
                    </Link>
                    <Link className="dropdown-item" to="/facultyCourse">
                      Faculty to Course
                    </Link>
                  </div>
                </div>
              </li>
            )}
            {user && user.role === "admin" && (
              <li className="nav-item">
                <div className="dropdown" onClick={this.toggleOpen2}>
                  <button
                    className="btn b-0 text-dark dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                  >
                    View
                  </button>
                  <div
                    className={menuClass2}
                    aria-labelledby="dropdownMenuButton"
                  >
                    <Link className="dropdown-item" to="/allStudents">
                      All Students
                    </Link>
                    <Link className="dropdown-item" to="/allFaculties">
                      All Faculties
                    </Link>
                  </div>
                </div>
              </li>
            )}
            {user && user.role === "student" && (
              <li className="nav-item">
                <Link className="nav-link text-dark" to="/studentDetails">
                  Student Details
                </Link>
              </li>
            )}
            {user && user.role === "student" && (
              <li className="nav-item">
                <Link className="nav-link text-dark" to="/allClasses">
                  All Classes
                </Link>
              </li>
            )}
            {user && user.role === "student" && (
              <li className="nav-item">
                <Link className="nav-link text-dark" to="/courseStudent">
                  All Course
                </Link>
              </li>
            )}
            {user && user.role === "faculty" && (
              <li className="nav-item">
                <Link className="nav-link text-dark" to="/coursesAssigned">
                  Courses
                </Link>
              </li>
            )}
            {user && user.role === "faculty" && (
              <li className="nav-item">
                <div className="dropdown" onClick={this.toggleOpen3}>
                  <button
                    className="btn b-0 text-dark dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                  >
                    Class Details
                  </button>
                  <div
                    className={menuClass3}
                    aria-labelledby="dropdownMenuButton"
                  >
                    <Link className="dropdown-item" to="/scheduleClass">
                      Schedule a class
                    </Link>
                    <Link className="dropdown-item" to="/scheduledClasses">
                      All Scheduled Classes
                    </Link>
                  </div>
                </div>
              </li>
            )}
          </ul>
          <ul className="navbar-nav ml-auto">
            {user && (
              <li className="nav-item">
                <Link className="nav-link text-dark">
                  Welcome {user.role === "admin" ? "Admin" : user.name}
                </Link>
              </li>
            )}
            {user && (
              <li className="nav-item">
                <Link className="nav-link text-dark" to="/logout">
                  Logout
                </Link>
              </li>
            )}
            {!user && (
              <li className="nav-item">
                <Link className="nav-link" to="login">
                  Login
                </Link>
              </li>
            )}
          </ul>
        </div>
      </nav>
    );
  }
}
export default NavBar;
