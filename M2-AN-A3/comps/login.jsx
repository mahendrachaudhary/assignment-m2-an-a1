import React, { Component } from "react";
import auth from "../services/authServices";
import httpServices from "../services/httpServices";
class Login extends Component {
  state = {
    loginDetail: { email: "", password: "" },
    errors: {},
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.loginDetail[input.name] = input.value;
    console.log(s1.loginDetail);
    this.handleValidate(e);
    this.setState(s1);
  };
  async login(url, obj) {
    try {
      let response = await httpServices.post(url, obj);
      let { data } = response;
      console.log(data);
      auth.login(data);
      data.role === "admin"
        ? (window.location = "/admin")
        : data.role === "student"
        ? (window.location = "/student")
        : (window.location = "/faculty");
    } catch (ex) {
      if (ex.response && ex.response.status === 500) {
        alert("Enter the valid login details !!");
      }
    }
  }
  handleSubmit = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    let errors = this.validateAll();
    if (this.isValid(errors)) {
      this.login("/login", this.state.loginDetail);
    } else {
      s1.errors = errors;
      this.setState(s1);
    }
  };
  isFormValid = () => {
    let errors = this.validateAll();
    return this.isValid(errors);
  };
  isValid = (errors) => {
    let keys = Object.keys(errors);
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
    return count === 0;
  };
  validateAll = () => {
    let { email, password } = this.state.loginDetail;
    let errors = {};
    errors.email = this.validateEmail(email);
    errors.password = this.validatePassword(password);
    return errors;
  };
  handleValidate = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    switch (input.name) {
      case "email":
        s1.errors.email = this.validateEmail(input.value);
        break;
      case "password":
        s1.errors.password = this.validatePassword(input.value);
        break;
      default:
        break;
    }
  };
  validateEmail = (email) => (email ? "" : "Email is required");
  validatePassword = (password) =>
    password && password.length > 6
      ? ""
      : "Password must be of atleast 7 characters";
  render() {
    const { errors, loginDetail } = this.state;
    let { email, password } = loginDetail;
    return (
      <div className="container text-center">
        <h2>Login</h2>
        <div className="row">
          <div className="col-1"></div>
          <div className="col-2">
            Email<b className="text-danger">*</b>
          </div>
          <div className="form-group col-6">
            <input
              className="form-control"
              type="text"
              name="email"
              value={email}
              placeholder="Enter your email"
              onChange={this.handleChange}
            />
            <span className="text-muted">
              <small>We will not share your user name with anyone else.</small>
            </span>
            <br />
            {errors.email ? (
              <div
                className="text-danger col-12"
                style={{ backgroundColor: "pink" }}
              >
                {errors.email}
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="col-3"></div>
          <div className="col-1"></div>
          <div className="col-2">
            Password<b className="text-danger">*</b>
          </div>
          <div className="col-6 form-group">
            <input
              className="form-control"
              type="password"
              name="password"
              value={password}
              placeholder="Enter your Password"
              onChange={this.handleChange}
            />
            {errors.password ? (
              <div
                className="text-danger col-12"
                style={{ backgroundColor: "pink" }}
              >
                {errors.password}
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="col-3"></div>
        </div>
        <button
          className="btn btn-primary mt-1 btn-sm"
          disabled={!this.isFormValid()}
          onClick={this.handleSubmit}
        >
          Login
        </button>
      </div>
    );
  }
}
export default Login;
