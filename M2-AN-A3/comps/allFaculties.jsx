import React, { Component } from "react";
import httpServices from "../services/httpServices";
import queryString from "query-string";
import LeftPanel from "./leftPanel";
class AllFaculties extends Component {
  state = {
    data: {},
    options: [
      "ANGULAR",
      "JAVASCRIPT",
      "REACT",
      "BOOTSTRAP",
      "CSS",
      "REST AND MICROSERVICES",
      "NODE",
    ],
  };
  async fetchData() {
    let queryParams = queryString.parse(this.props.location.search);
    let searchStr = this.makeSearchString(queryParams);
    let response = await httpServices.get(
      `/getFaculties?${!searchStr ? "page=1" : searchStr}`
    );
    let { data } = response;
    this.setState({ data: data });
  }
  componentDidMount() {
    this.fetchData();
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) this.fetchData();
  }
  handleOptionChange = (options) => {
    options.page = 1;
    console.log(options);
    let url = "/allfaculties";
    this.callURL(url, options);
  };
  handlePage = (incr) => {
    let queryParams = queryString.parse(this.props.location.search);
    let { page = "1" } = queryParams;
    let newPage = +page + incr;
    queryParams.page = newPage;
    this.callURL("/allfaculties", queryParams);
  };
  callURL = (url, options) => {
    let searchString = this.makeSearchString(options);
    this.props.history.push({
      pathname: url,
      search: searchString,
    });
  };
  makeSearchString = (options) => {
    let { page, course } = options;
    let searchStr = "";
    searchStr = this.addToQueryString(searchStr, "page", page);
    searchStr = this.addToQueryString(searchStr, "course", course);
    return searchStr;
  };
  addToQueryString = (str, paramName, paramValue) =>
    paramValue
      ? str
        ? `${str}&${paramName}=${paramValue}`
        : `${paramName}=${paramValue}`
      : str;
  render() {
    const { data = {}, options } = this.state;
    const { items = [], page, totalItems, totalNum } = data;
    const queryParams = queryString.parse(this.props.location.search);
    let startIndex = (page - 1) * 3;
    let endIndex =
      totalItems < startIndex + totalItems + 1
        ? startIndex + totalItems - 1
        : totalNum;
    return (
      <div className="container">
        <br />
        <div className="row">
          <div className="col-3">
            <LeftPanel
              course={queryParams}
              options={options}
              onOptionChange={this.handleOptionChange}
            />
          </div>
          <div className="col-9">
            <h4>All Faculties</h4>
            {startIndex + 1} - {endIndex + 1} of {totalNum}
            <div className="row bg-light">
              <div className="col-4">Id</div>
              <div className="col-4">Name</div>
              <div className="col-4">Courses</div>
            </div>
            {items.map((i, index) => (
              <div className="row border-bottom boder-white bg-warning">
                <div className="col-1">{i.id}</div>
                <div className="col-2">{i.name}</div>
                <div className="col-3">{i.courses}</div>
              </div>
            ))}
            <div className="row">
              <div className="col-2">
                {page > 1 ? (
                  <button
                    className="btn btn-dark text-white m-1 btn-sm"
                    onClick={() => this.handlePage(-1)}
                  >
                    Prev
                  </button>
                ) : (
                  ""
                )}
              </div>
              <div className="col-8"></div>
              <div className="col-2">
                {endIndex + 1 < totalNum ? (
                  <button
                    className="btn btn-dark text-white m-1 btn-sm"
                    onClick={() => this.handlePage(1)}
                  >
                    Next
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default AllFaculties;
